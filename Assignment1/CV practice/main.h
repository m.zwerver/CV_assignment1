#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

#include <sstream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <tuple>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include "camera_calibration.h"

void slidesPractice();

void print_matrix(Mat_<double> m);

void worldToImage(vector<Point3f> world_coords, Settings s, Mat_<double> camera_intrinsics, Mat_<double> dist_coefficients, vector<Point2f> pointBuf, Mat_<double> extrinsic_parameters, vector<Point2f>& image_output);

void drawWorld3Daxis(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> extrinsic_parameters, Mat_<double> dist_coefficients, Mat view);

void drawWorld3DCube(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> extrinsic_parameters, Mat_<double> dist_coefficients, Mat view);

void readSettingsAndDraw(Settings s, vector<Point2f> pointBuf, Mat view, bool already_calibrated);

void cameraStuff(bool already_calibrated);

void calcMatrices(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> dist_coefficients);

void DrawObj(vector<Point2f> output, Mat view, vector<Point2i> connections);

int main();

Mat rvec, tvec;
vector<Point3f> objectPoints;
int ss;
bool read_settings;
Mat_<double> camera_intrinsics = Mat_<double>(3, 3);
Mat_<double>  dist_coefficients = Mat_<double>(5, 1);
