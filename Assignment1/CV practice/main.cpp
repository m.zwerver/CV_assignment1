
#include "main.h"

using namespace cv;
using namespace std;

//Prints given matrix (for debug purposes)
void print_matrix(Mat_<double> m)
{
	for (int i = 0; i < m.rows; i++)
	{
		for (int j = 0; j < m.cols; j++)
			cout << m.at<double>(i, j) << " ";
		cout << endl;
	}
	cout << endl;
}

//Converts world coordinates to image coordinates (gives slightly different results from the opencv function because distortion coeficients are not taken into account)
void worldToImage(vector<Point3f> world_coords, Settings s, Mat_<double> camera_intrinsics, Mat_<double> dist_coefficients, vector<Point2f> pointBuf, Mat_<double> extrinsic_parameters, vector<Point2f>& image_output)
{
	calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints, s.calibrationPattern); //objectpoints are 3D coordinates of corners, z are set to 0!
	solvePnP(objectPoints, pointBuf, camera_intrinsics, dist_coefficients, rvec, tvec); //pointbuf is 2D image coordinates
	
	Mat_<double> rot_mat = Mat_<double>(3, 3); //3x3
	Rodrigues(rvec, rot_mat);

	//construct [R|t]
	Mat_<double> R_t = Mat_<double>::eye(4, 4);
	//add tvec
	R_t.at<double>(0, 3) = tvec.at<double>(0);
	R_t.at<double>(1, 3) = tvec.at<double>(1);
	R_t.at<double>(2, 3) = tvec.at<double>(2);
	//add rvec
	R_t.at<double>(0, 0) = rot_mat.at<double>(0, 0);
	R_t.at<double>(0, 1) = rot_mat.at<double>(0, 1);
	R_t.at<double>(0, 2) = rot_mat.at<double>(0, 2);

	R_t.at<double>(1, 0) = rot_mat.at<double>(1, 0);
	R_t.at<double>(1, 1) = rot_mat.at<double>(1, 1);
	R_t.at<double>(1, 2) = rot_mat.at<double>(1, 2);

	R_t.at<double>(2, 0) = rot_mat.at<double>(2, 0);
	R_t.at<double>(2, 1) = rot_mat.at<double>(2, 1);
	R_t.at<double>(2, 2) = rot_mat.at<double>(2, 2);
	

	for (int i = 0; i < world_coords.size(); i++)
	{
		Mat_<double> world_coor = Mat_<double>(4,1);
		world_coor.at<double>(0,0) = (world_coords.at(i).x);
		world_coor.at<double>(1,0) = (world_coords.at(i).y);
		world_coor.at<double>(2,0) = (world_coords.at(i).z);
		world_coor.at<double>(3,0) = (1.0f);
		
		//calc image coordinates
		Mat_<double> camera_coor_mat = R_t * world_coor;
		camera_coor_mat.pop_back(1);

		Mat_<double> image_coor_mat = camera_intrinsics * camera_coor_mat;

		//convert from homogeneous coordinates back to regular image coordinates by dividing by the 'z' value of the vector
		Point2f image_coor = Point2f(image_coor_mat(0) / image_coor_mat(2), image_coor_mat(1) / image_coor_mat(2));
		
		image_output.push_back(image_coor);
	}
	return;
}

//Draws an object based on the object points and their connections
void DrawObj(vector<Point2f> output, Mat view, vector<Point2i> connections)
{
	for (int i = 0; i < connections.size(); ++i)
	{
		line(view, output.at(connections[i].x), output.at(connections[i].y), Scalar(255, 255, 0), 1);
	}
}

//Draws a 3D axis system with its origin on the world origin.
void drawWorld3Daxis(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> extrinsic_parameters, Mat_<double> dist_coefficients, Mat view)
{
	calcMatrices(s, pointBuf, camera_intrinsics, dist_coefficients);
	
	//axis points in world coordinates
	vector<Point3f> axis_points;
	axis_points.push_back(Point3f(0, 0, 0));
	axis_points.push_back(Point3f((float)ss * 3, 0, 0));
	axis_points.push_back(Point3f(0, (float)ss * 3, 0));
	axis_points.push_back(Point3f(0, 0, (float)-ss * 3));

	//get axis points on image
	vector<Point2f> axis_output;
	//projectPoints(axis_points, rvec, tvec, camera_intrinsics, dist_coefficients, axis_output);
	worldToImage(axis_points, s, camera_intrinsics, dist_coefficients, pointBuf, extrinsic_parameters, axis_output);
	
	//draw x, y and z axis at world origin
	line(view, axis_output.at(0), axis_output.at(1), Scalar(0, 255, 0), 2);
	line(view, axis_output.at(0), axis_output.at(2), Scalar(0, 0, 255), 2);
	line(view, axis_output.at(0), axis_output.at(3), Scalar(255, 0, 0), 2);
}


//Draws a 3D cube with its origin on the world origin.
void drawWorld3DCube(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> extrinsic_parameters, Mat_<double> dist_coefficients, Mat view)
{
	calcMatrices(s, pointBuf, camera_intrinsics, dist_coefficients);

	//cube points in world coordinates
	vector<Point3f> cube_points;
	cube_points.push_back(Point3f(0, 0, 0));
	cube_points.push_back(Point3f(0, 0, (float)-ss * 2));
	cube_points.push_back(Point3f(0, (float)ss * 2, 0));
	cube_points.push_back(Point3f(0, (float)ss * 2, (float)-ss * 2));
	cube_points.push_back(Point3f((float)ss * 2, 0, 0));
	cube_points.push_back(Point3f((float)ss * 2, 0, (float)-ss * 2));
	cube_points.push_back(Point3f((float)ss * 2, (float)ss * 2, 0));
	cube_points.push_back(Point3f((float)ss * 2, (float)ss * 2, (float)-ss * 2));

	//get cube points on image
	vector<Point2f> cube_output;
	//projectPoints(cube_points, rvec, tvec, camera_intrinsics, dist_coefficients, cube_output);
	worldToImage(cube_points, s, camera_intrinsics, dist_coefficients, pointBuf, extrinsic_parameters, cube_output);

	vector<Point2i> connections;
	connections.push_back(Point2i(0, 1));
	connections.push_back(Point2i(0, 2));
	connections.push_back(Point2i(0, 4));
	connections.push_back(Point2i(7, 3));
	connections.push_back(Point2i(7, 5));
	connections.push_back(Point2i(7, 6));
	connections.push_back(Point2i(2, 6));
	connections.push_back(Point2i(4, 6));
	connections.push_back(Point2i(3, 1));
	connections.push_back(Point2i(3, 2));
	connections.push_back(Point2i(5, 1));
	connections.push_back(Point2i(4, 5));

	//draw lines between cube points
	DrawObj(cube_output, view, connections);
}

/*
If camera has been calibrated:
Reads in the settings from the file generated by the camera calibration if not done already.
Draws a 3D axis system with its origin on the world origin.
Draws a 3D cube with its origin on the world origin.
*/
void readSettingsAndDraw(Settings s, vector<Point2f> pointBuf, Mat view, bool already_calibrated)
{
	Mat_<double>  extrinsic_parameters = Mat_<double>(25, 6);

	if (already_calibrated)
	{
		//read in camera intrinsics, distortion coefficients and extrinsics
		const String inputSettingsFile = "data/out_camera_data.xml";
		FileStorage fs(inputSettingsFile, FileStorage::READ); // Read the settings
		if (!fs.isOpened())
		{
			cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
			return;
		}
		
		if (!read_settings)
		{
			fs["camera_matrix"] >> camera_intrinsics; //3x3
			fs["distortion_coefficients"] >> dist_coefficients;//5x1
			read_settings = true;
		}

		fs["extrinsic_parameters"] >> extrinsic_parameters;
		fs.release();
		
		drawWorld3Daxis(s, pointBuf, camera_intrinsics, extrinsic_parameters, dist_coefficients, view);
		drawWorld3DCube(s, pointBuf, camera_intrinsics, extrinsic_parameters, dist_coefficients, view);
	}
}

/*
Function copied from camera_calibration.cpp, made some slight adjustments
Reads in input settings
Shows input on screen
Calibrates camera when 'g' is pressed
Draws inner chessboard corners on the screen
Calls function that draw axis system and cube on screen
*/
void cameraStuff(bool already_calibrated)
{
	//read in settings
	Settings s;
	const String inputSettingsFile = "data/in_VIDEO.xml"; // used square size in mm
	FileStorage fs(inputSettingsFile, FileStorage::READ); // Read the settings
	if (!fs.isOpened())
	{
		cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
		return;
	}
	fs["Settings"] >> s; 
	fs.release();                                         // close Settings file


	if (!s.goodInput)
	{
		cout << "Invalid input detected. Application stopping. " << endl;
		return;
	}

	//other stuff
	vector<vector<Point2f> > imagePoints;
	Mat cameraMatrix, distCoeffs;
	Size imageSize;
	int mode = s.inputType == Settings::IMAGE_LIST ? CAPTURING : DETECTION;
	clock_t prevTimestamp = 0;
	const Scalar RED(0, 0, 255), GREEN(0, 255, 0);
	const char ESC_KEY = 27;


	//camera stuff
	VideoCapture stream1(0);   //0 is the id of video device.0 if you have only one camera.

	if (!stream1.isOpened()) 
	{ //check if video device has been initialised
		cout << "cannot open camera";
	}

	for (;;)
	{
		Mat view;
		bool blinkOutput = false;

		view = s.nextImage();

		//-----  If no more image, or got enough, then stop calibration and show result -------------
		if (mode == CAPTURING && imagePoints.size() >= (size_t)s.nrFrames)
		{
			if (runCalibrationAndSave(s, imageSize, cameraMatrix, distCoeffs, imagePoints))
			{
				mode = CALIBRATED;
				already_calibrated = true;
			}
			else
				mode = DETECTION;
		}
		if (view.empty())          // If there are no more images stop the loop
		{
			// if calibration threshold was not reached yet, calibrate now
			if (mode != CALIBRATED && !imagePoints.empty())
				runCalibrationAndSave(s, imageSize, cameraMatrix, distCoeffs, imagePoints);
			break;
		}
		//! [get_input]

		imageSize = view.size();  // Format input image.
		if (s.flipVertical)    flip(view, view, 0);

		//! [find_pattern]
		vector<Point2f> pointBuf;

		bool found;

		int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;

		if (!s.useFisheye) 
		{
			// fast check erroneously fails with high distortions like fisheye
			chessBoardFlags |= CALIB_CB_FAST_CHECK;
		}

		switch (s.calibrationPattern) // Find feature points on the input format
		{
		case Settings::CHESSBOARD:
			found = findChessboardCorners(view, s.boardSize, pointBuf, chessBoardFlags);
			break;
		case Settings::CIRCLES_GRID:
			found = findCirclesGrid(view, s.boardSize, pointBuf);
			break;
		case Settings::ASYMMETRIC_CIRCLES_GRID:
			found = findCirclesGrid(view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID);
			break;
		default:
			found = false;
			break;
		}
		//! [find_pattern]
		//! [pattern_found]
		if (found)                // If done with success,
		{
			// improve the found corners' coordinate accuracy for chessboard
			if (s.calibrationPattern == Settings::CHESSBOARD)
			{
				Mat viewGray;
				cvtColor(view, viewGray, COLOR_BGR2GRAY);
				cornerSubPix(viewGray, pointBuf, Size(11, 11),
					Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
			}

			if (mode == CAPTURING &&  // For camera only take new samples after delay time
				(!s.inputCapture.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC))
			{
				imagePoints.push_back(pointBuf);
				prevTimestamp = clock();
				blinkOutput = s.inputCapture.isOpened();
			}

			// Draw the corners.
			drawChessboardCorners(view, s.boardSize, Mat(pointBuf), found);
			readSettingsAndDraw(s, pointBuf, view, already_calibrated);
		}
		//! [pattern_found]
		//----------------------------- Output Text ------------------------------------------------
		//! [output_text]
		string msg = (mode == CAPTURING) ? "100/100" :
			mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
		int baseLine = 0;
		Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
		Point textOrigin(view.cols - 2 * textSize.width - 10, view.rows - 2 * baseLine - 10);

		if (mode == CAPTURING)
		{
			if (s.showUndistorsed)
				msg = format("%d/%d Undist", (int)imagePoints.size(), s.nrFrames);
			else
				msg = format("%d/%d", (int)imagePoints.size(), s.nrFrames);
		}

		putText(view, msg, textOrigin, 1, 1, mode == CALIBRATED ? GREEN : RED);

		if (blinkOutput)
			bitwise_not(view, view);
		//! [output_text]
		//------------------------- Video capture  output  undistorted ------------------------------
		//! [output_undistorted]
		if (mode == CALIBRATED && s.showUndistorsed)
		{
			Mat temp = view.clone();
			if (s.useFisheye)
				cv::fisheye::undistortImage(temp, view, cameraMatrix, distCoeffs);
			else
				undistort(temp, view, cameraMatrix, distCoeffs);
		}
		//! [output_undistorted]
		//------------------------------ Show image and check for input commands -------------------
		//! [await_input]
		imshow("Image View", view);
		char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

		if (key == ESC_KEY)
			break;

		if (key == 'u' && mode == CALIBRATED)
			s.showUndistorsed = !s.showUndistorsed;

		if (s.inputCapture.isOpened() && key == 'g')
		{
			mode = CAPTURING;
			imagePoints.clear();
		}
		//! [await_input]
	}

	// -----------------------Show the undistorted image for the image list ------------------------
	//! [show_results]
	if (s.inputType == Settings::IMAGE_LIST && s.showUndistorsed)
	{
		Mat view, rview, map1, map2;

		if (s.useFisheye)
		{
			Mat newCamMat;
			fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imageSize,
				Matx33d::eye(), newCamMat, 1);
			fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Matx33d::eye(), newCamMat, imageSize,
				CV_16SC2, map1, map2);
		}
		else
		{
			initUndistortRectifyMap(
				cameraMatrix, distCoeffs, Mat(),
				getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize,
				CV_16SC2, map1, map2);
		}

		for (size_t i = 0; i < s.imageList.size(); i++)
		{
			view = imread(s.imageList[i], IMREAD_COLOR);
			if (view.empty())
				continue;
			remap(view, rview, map1, map2, INTER_LINEAR);
			imshow("Image View", rview);
			char c = (char)waitKey();
			if (c == ESC_KEY || c == 'q' || c == 'Q')
				break;
		}
	}
	//! [show_results]

}

//Gets rotation vector (rvec) end translation vector (tvec) for current frame based on 3D world points and corresponding image points
void calcMatrices(Settings s, vector<Point2f> pointBuf, Mat_<double> camera_intrinsics, Mat_<double> dist_coefficients)
{
	ss = (int)s.squareSize;
	calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints, s.calibrationPattern); //objectpoints are 3D coordinates of corners, z are set to 0!
	solvePnP(objectPoints, pointBuf, camera_intrinsics, dist_coefficients, rvec, tvec); //pointbuf is 2D image coordinates
}

int main()
{
	read_settings = false;
	bool already_calibrated = false; //for debug purposes, since the calibration file can be reused
	cameraStuff(already_calibrated);

	waitKey(0);
	return 0;
}