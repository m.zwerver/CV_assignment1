/*
 * Scene3DRenderer.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: coert
 */

#include "Scene3DRenderer.h"

#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <stddef.h>
#include <string>
#include <iostream>

#include "../utilities/General.h"

using namespace std;
using namespace cv;

namespace nl_uu_science_gmt
{

/**
 * Constructor
 * Scene properties class (mostly called by Glut)
 */
Scene3DRenderer::Scene3DRenderer(
		Reconstructor &r, const vector<Camera*> &cs) :
				m_reconstructor(r),
				m_cameras(cs),
				m_num(4),
				m_sphere_radius(1850)
{
	m_width = 640;
	m_height = 480;
	m_quit = false;
	m_paused = false;
	m_rotate = false;
	m_camera_view = true;
	m_show_volume = true;
	m_show_grd_flr = true;
	m_show_cam = true;
	m_show_org = true;
	m_show_arcball = false;
	m_show_info = true;
	m_fullscreen = false;

	// Read the checkerboard properties (XML)
	FileStorage fs;
	fs.open(m_cameras.front()->getDataPath() + ".." + string(PATH_SEP) + General::CBConfigFile, FileStorage::READ);
	if (fs.isOpened())
	{
		fs["CheckerBoardWidth"] >> m_board_size.width;
		fs["CheckerBoardHeight"] >> m_board_size.height;
		fs["CheckerBoardSquareSize"] >> m_square_side_len;
	}
	fs.release();

	m_current_camera = 0;
	m_previous_camera = 0;

	m_number_of_frames = m_cameras.front()->getFramesAmount();
	m_current_frame = 0;
	m_previous_frame = -1;

	const int H = 0;
	const int S = 0;
	const int V = 0;
	m_h_threshold = H;
	m_ph_threshold = H;
	m_s_threshold = S;
	m_ps_threshold = S;
	m_v_threshold = V;
	m_pv_threshold = V;



	createFloorGrid();
	setTopView();


}

/**
 * Deconstructor
 * Free the memory of the floor_grid pointer vector
 */
Scene3DRenderer::~Scene3DRenderer()
{
	for (size_t f = 0; f < m_floor_grid.size(); ++f)
		for (size_t g = 0; g < m_floor_grid[f].size(); ++g)
			delete m_floor_grid[f][g];
}

/**
 * Process the current frame on each camera
 */
bool Scene3DRenderer::processFrame()
{
	for (size_t c = 0; c < m_cameras.size(); ++c)
	{
		if (m_current_frame == m_previous_frame + 1)
		{
			m_cameras[c]->advanceVideoFrame();
		}
		else if (m_current_frame != m_previous_frame)
		{
			m_cameras[c]->getVideoFrame(m_current_frame);
		}
		assert(m_cameras[c] != NULL);

		processForeground(m_cameras[c]);
	}
	if (first_frame == true)
	{
		getBestHSVsmart(m_cameras);
		getBestErodeDilate(m_cameras[0]);
		first_frame = false;
	}
	return true;
}

//for given hsv thresholds, returns the percentage of matching pixels between the extracted foreground and the corresponding ground truth
float testHSV(int h, int s, int v, vector<Mat> channels, Camera* camera, Mat best_foreground)
{
	// Background subtraction H
	Mat tmp, foreground, background;

	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, s, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, v, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	//compare found foreground with true foreground
	Mat difference;
	absdiff(foreground, best_foreground, difference);
	int mistakes = countNonZero(difference);
	float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

	//cout << "for: h = " << h << ", s = " << s << ", v = " << v << ", mistakes = " << mistakes << ", match = " << hit_per << endl;
	return hit_per;

}

//guesses hsv values for the first camera with corresponding ground truth
void guessHSV(int& best_h, int& best_s, int& best_v, int tries, vector<Mat> channels, Camera* camera, Mat best_foreground)
{
	float best_per = 0;
	int current_best_value;
	for (int i = 0; i < tries; i++)
	{
		int hsv_value = (255.0 / tries) * i;
		
		float hit_per = testHSV(hsv_value, hsv_value, hsv_value, channels, camera, best_foreground);
		if (hit_per > best_per)
		{
			best_per = hit_per;
			current_best_value = hsv_value;
		}

	}

	best_h = current_best_value;
	best_s = current_best_value;
	best_v = current_best_value;
}

//calculates the optimal hsv values by guessing hsv values and then adjusting the values in the direction of the highest gain
void Scene3DRenderer::getBestHSVsmart(vector<Camera*> cameras)
{

	Mat best_foreground_1 = imread(cameras[0]->getDataPath() + "true_foreground1.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat best_foreground_2 = imread(cameras[1]->getDataPath() + "true_foreground2.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat best_foreground_3 = imread(cameras[2]->getDataPath() + "true_foreground3.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat best_foreground_4 = imread(cameras[3]->getDataPath() + "true_foreground4.png", CV_LOAD_IMAGE_GRAYSCALE);
	

	imshow("ground truth", best_foreground_1);
	Mat current_frame1, current_frame2, current_frame3, current_frame4;

	assert(!cameras[0]->getFrame().empty());
	cvtColor(cameras[0]->getFrame(), current_frame1, CV_BGR2HSV);  // from BGR to HSV color space
	assert(!cameras[1]->getFrame().empty());
	cvtColor(cameras[1]->getFrame(), current_frame2, CV_BGR2HSV);  // from BGR to HSV color space
	assert(!cameras[2]->getFrame().empty());
	cvtColor(cameras[2]->getFrame(), current_frame3, CV_BGR2HSV);  // from BGR to HSV color space
	assert(!cameras[3]->getFrame().empty());
	cvtColor(cameras[3]->getFrame(), current_frame4, CV_BGR2HSV);  // from BGR to HSV color space


	vector<Mat> channels1, channels2, channels3, channels4;
	split(current_frame1, channels1);  // Split the HSV-channels for further analysis
	split(current_frame2, channels2);  // Split the HSV-channels for further analysis
	split(current_frame3, channels3);  // Split the HSV-channels for further analysis
	split(current_frame4, channels4);  // Split the HSV-channels for further analysis


	int tries = 0;
	int best_h;
	int best_s;
	int best_v;

	guessHSV(best_h, best_s, best_v, 10, channels1, cameras[0], best_foreground_1);

	float best_hit_rate = 0;
	bool progress = true;
	int max_tries = 10; //this one is important for escaping local minima (if there are any). in this particular case, 9 is the minimum to get the optimal solution
	int step_size = 1;

	while (progress)
	{
		int best_h_round;
		int best_s_round;
		int best_v_round;
		
		float best_per_round = best_hit_rate;

		for (int h_step = -1; h_step <= 1; h_step += 1)
		{
			for (int s_step = -1; s_step <= 1; s_step += 1)
			{
				for (int v_step = -1; v_step <= 1; v_step += 1)
				{
					int h = best_h + h_step * step_size;
					int s = best_s + s_step * step_size;
					int v = best_v + v_step * step_size;

					if (!((h < 0) || (h > 255) || (s < 0) || (s > 255) || (v < 0) || (v > 255)))
					{
						float hit_per = testHSV(h, s, v, channels1, cameras[0], best_foreground_1) + testHSV(h, s, v, channels2, cameras[1], best_foreground_2) + testHSV(h, s, v, channels3, cameras[2], best_foreground_3) + testHSV(h, s, v, channels4, cameras[3], best_foreground_4);
						tries++;
						if (hit_per > best_per_round)
						{
							best_h_round = h;
							best_s_round = s;
							best_v_round = v;
							best_per_round = hit_per;
						}
					}
				}
			}
		}

		if (best_per_round > best_hit_rate)
		{
			best_h = best_h_round;
			best_s = best_s_round;
			best_v = best_v_round;
			best_hit_rate = best_per_round;
			step_size = 1;
		}
		else
		{
			step_size++;
			if(step_size > max_tries)
				progress = false;
		}
	}
	cout << "best hsv: " << best_h << ", " << best_s << ", " << best_v << ", rate: " << best_hit_rate/4 << ", tries: " << tries << endl;
	m_h_threshold = best_h;
	m_ph_threshold = best_h;
	m_s_threshold = best_s;
	m_ps_threshold = best_s;
	m_v_threshold = best_v;
	m_pv_threshold = best_v;

	createTrackbar("Frame", VIDEO_WINDOW, &m_current_frame, m_number_of_frames - 2);
	createTrackbar("H", VIDEO_WINDOW, &m_h_threshold, 255);
	createTrackbar("S", VIDEO_WINDOW, &m_s_threshold, 255);
	createTrackbar("V", VIDEO_WINDOW, &m_v_threshold, 255);
}

//Function which finds the best amount of iterates to use for erode and dilate. 
//Also finds the best size of the shape to erode and dilate with.
//It uses the optimal values hsv values found in the previous optimize function.
//Because the range of the values is small we brute force it.
//Erosion and dilation does not have a big inpact such as the optimal hsv values, it only translates to a 0,05% improvement on average.
void Scene3DRenderer::getBestErodeDilate(Camera * camera)
{
	String path = camera->getDataPath();
	cout << path;
	Mat best_foreground = imread(path + "true_foreground1.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat current_frame;

	assert(!camera->getFrame().empty());
	cvtColor(camera->getFrame(), current_frame, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels, channels_best;
	split(current_frame, channels);  // Split the HSV-channels for further analysis

	int h = m_h_threshold;
	int s = m_s_threshold;
	int v = m_v_threshold;
	float best_hit_rate = 0;


	int best_erode_it = 0;
	int best_dilate_it = 0;
	int best_erode = 0;
	int best_dilate = 0;

	
	// Background subtraction H
	Mat tmp, foreground, background;

	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, s, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, v, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	for (int e_iterate = 0; e_iterate < 5; e_iterate++)
		for (int d_iterate = 0; d_iterate < 5; d_iterate++)
			for(int e =0; e < 5;e++)
				for(int d = 0; d < 5; d++)
				{
					int ane = e;
					int and = d;
					Mat elemente = getStructuringElement(MORPH_RECT, Size(ane + 1, ane + 1), Point(ane, ane));
					Mat elementd = getStructuringElement(MORPH_RECT, Size(and + 1, and + 1), Point(and, and));
					for(int i =0; i < e_iterate;i++)
						erode(foreground, tmp, elemente);
					for (int i = 0; i < d_iterate; i++)
						dilate(tmp, tmp, elementd);

					//compare found foreground with true foreground
					Mat difference;
					absdiff( best_foreground,tmp, difference);
					int mistakes = countNonZero(difference);
					float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

					if (hit_per > best_hit_rate)
					{
						best_erode_it = e_iterate;
						best_dilate_it = d_iterate;
						best_erode = e;
						best_dilate = d;
						best_hit_rate = hit_per;
					}
				}	
	cout << "best: e_iterate = " << best_erode_it << ", d_iterate = " << best_dilate_it  << ", e =" << best_erode << ", d =" << best_dilate << ", match = " << best_hit_rate << endl;

	erodeSize = best_erode;
	erode_iterations = best_erode_it;
	dilateSize = best_dilate;
	dilate_iterations = best_dilate_it;
}

//gets the optimal hsv values using brute force (don't run, takes forever)
void Scene3DRenderer::getBestHSV(Camera* camera)
{
	String path = camera->getDataPath();
	cout << path;
	Mat best_foreground = imread(path + "true_foreground1.png", CV_LOAD_IMAGE_GRAYSCALE);
	imshow("ground truth", best_foreground);
	Mat current_frame;

	assert(!camera->getFrame().empty());
	cvtColor(camera->getFrame(), current_frame, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels, channels_best;
	split(current_frame, channels);  // Split the HSV-channels for further analysis

	int best_h = 0;
	int best_s = 0;
	int best_v = 0;
	float best_hit_rate = 0;

	for (int h = 0; h < 255; h++)
	{
		for (int s = 0; s < 255; s++)
		{
			for (int v = 0; v < 255; v++)
			{
				// Background subtraction H
				Mat tmp, foreground, background;

				absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
				threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

				// Background subtraction S
				absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
				threshold(tmp, background, s, 255, CV_THRESH_BINARY);
				bitwise_and(foreground, background, foreground);

				// Background subtraction V
				absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
				threshold(tmp, background, v, 255, CV_THRESH_BINARY);
				bitwise_or(foreground, background, foreground);

				//compare found foreground with true foreground
				Mat difference;
				absdiff(foreground, best_foreground, difference);
				int mistakes = countNonZero(difference);
				float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

				if (hit_per > best_hit_rate)
				{
					best_h = h;
					best_s = s;
					best_v = v;
					best_hit_rate = hit_per;
				}

			}
		}
	}
	cout << "Best:for: h = " << best_h << ", s = " << best_s << ", v = " << best_v << ", match = " << best_hit_rate/2 << endl;
	m_h_threshold = best_h;
	m_ph_threshold = best_h;
	m_s_threshold = best_s;
	m_ps_threshold = best_s;
	m_v_threshold = best_v;
	m_pv_threshold = best_v;
}

/**
 * Separate the background from the foreground
 * ie.: Create an 8 bit image where only the foreground of the scene is white (255)
 */
void Scene3DRenderer::processForeground(Camera* camera)
{
	//imwrite("image.png", camera->getFrame());
	assert(!camera->getFrame().empty());
	Mat hsv_image;
	cvtColor(camera->getFrame(), hsv_image, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels;
	split(hsv_image, channels);  // Split the HSV-channels for further analysis

	// Background subtraction H
	Mat tmp, foreground, background;
	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, m_h_threshold, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, m_s_threshold, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, m_v_threshold, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	// Improve the foreground image
	Mat elemente = getStructuringElement(MORPH_ELLIPSE, Size(erodeSize + 1, erodeSize + 1), Point(erodeSize, erodeSize));
	Mat elementd = getStructuringElement(MORPH_RECT, Size(dilateSize + 1, dilateSize + 1), Point(dilateSize, dilateSize));

	for (int i = 0; i < erode_iterations; i++)
		erode(foreground, foreground, elemente);
	for (int i = 0; i < dilate_iterations+1; i++)
		dilate(foreground, foreground, elementd);

	camera->setForegroundImage(foreground);
	imshow("foreground", foreground);

}

/**
 * Set currently visible camera to the given camera id
 */
void Scene3DRenderer::setCamera(
		int camera)
{
	m_camera_view = true;

	if (m_current_camera != camera)
	{
		m_previous_camera = m_current_camera;
		m_current_camera = camera;
		m_arcball_eye.x = m_cameras[camera]->getCameraPlane()[0].x;
		m_arcball_eye.y = m_cameras[camera]->getCameraPlane()[0].y;
		m_arcball_eye.z = m_cameras[camera]->getCameraPlane()[0].z;
		m_arcball_up.x = 0.0f;
		m_arcball_up.y = 0.0f;
		m_arcball_up.z = 1.0f;
	}
}

/**
 * Set the 3D scene to bird's eye view
 */
void Scene3DRenderer::setTopView()
{
	m_camera_view = false;
	if (m_current_camera != -1)
		m_previous_camera = m_current_camera;
	m_current_camera = -1;

	m_arcball_eye = vec(0.0f, 0.0f, 10000.0f);
	m_arcball_centre = vec(0.0f, 0.0f, 0.0f);
	m_arcball_up = vec(0.0f, 1.0f, 0.0f);
}

/**
 * Create a LUT for the floor grid
 */
void Scene3DRenderer::createFloorGrid()
{
	const int size = m_reconstructor.getSize() / m_num;
	const int z_offset = 3;

	// edge 1
	vector<Point3i*> edge1;
	for (int y = -size * m_num; y <= size * m_num; y += size)
		edge1.push_back(new Point3i(-size * m_num, y, z_offset));

	// edge 2
	vector<Point3i*> edge2;
	for (int x = -size * m_num; x <= size * m_num; x += size)
		edge2.push_back(new Point3i(x, size * m_num, z_offset));

	// edge 3
	vector<Point3i*> edge3;
	for (int y = -size * m_num; y <= size * m_num; y += size)
		edge3.push_back(new Point3i(size * m_num, y, z_offset));

	// edge 4
	vector<Point3i*> edge4;
	for (int x = -size * m_num; x <= size * m_num; x += size)
		edge4.push_back(new Point3i(x, -size * m_num, z_offset));

	m_floor_grid.push_back(edge1);
	m_floor_grid.push_back(edge2);
	m_floor_grid.push_back(edge3);
	m_floor_grid.push_back(edge4);
}

} /* namespace nl_uu_science_gmt */
