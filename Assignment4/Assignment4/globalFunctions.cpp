#pragma once

#include "globalFunctions.h"

using namespace std;
using namespace cv;

//Gets image id's from the bird images and generates their index for the bounding box
void getImageIds(string folder_path, vector<int>& image_ids, vector<cv::String>& image_paths, vector<int> indices)
{
	vector<cv::String> fn;
	glob(folder_path + "*.jpg", fn); //gets all path names in folder

	for (int i = 0; i < indices.size(); i++) //save image paths
	{
		image_paths.push_back(fn[indices[i]]);
	}

	for (int i = 0; i < fn.size(); i++) //removes path so only file name remains
	{
		string file_name = fn[i].substr(folder_path.length(), fn[i].length());
		fn[i] = file_name;
	}

	for (int i = 0; i < indices.size(); i++) //stores image_ids
	{
		string line;
		ifstream myfile("data/images.txt");
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				if (line.find(fn[indices[i]]) != std::string::npos)
				{
					int index = line.find(' ');
					int image_id = stoi(line.substr(0, index));
					image_ids.push_back(image_id);
				}
			}
			myfile.close();
		}
	}
}

//get bounding boxes of the birds according to their image id
void getImageBBs(vector<int> image_ids, vector<vector<int>>& bounding_boxes)
{
	for (int i = 0; i < image_ids.size(); i++)
	{
		string line;
		ifstream myfile("data/bounding_boxes.txt");
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				int index = line.find(' ');
				int image_id_file = stoi(line.substr(0, index));

				if (image_id_file == image_ids[i])
				{
					vector<int> bounding_box;
					for (int c = 0; c < 4; c++)
					{
						int index = line.find(' ');			//first space
						int index2 = line.find(' ', index + 1);	//second space
						int coordinate = stoi(line.substr(index + 1, index2 - 1));
						bounding_box.push_back(coordinate);
						if (c != 3)
							line = line.substr(index2, line.length());
					}
					bounding_boxes.push_back(bounding_box);
				}
			}
			myfile.close();
		}
	}
}
