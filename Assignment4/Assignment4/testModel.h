#pragma once

#include "stdafx.h"
#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "variables.h"
#include "globalFunctions.h"

using namespace std;
using namespace cv;


//here all functions used for testing the model
void loadSize(Size & size);
void get_svm_detector(Ptr<ml::SVM>& svm, vector< float > & hog_detector);
void drawLocations(Mat& img, const vector<Rect>& locations, vector<double> found_weights);
void orderBBs(vector<Rect>& locations, vector<double>& found_weights);
void nonMaxSuppression(vector<Rect>& locations, vector<double>& found_weights, int start = 0);
void getErrors(vector<Rect> locations, Rect correct_bb, vector<int>& error_matrix, bool is_bird);
void mirrorBoundingBox(vector<int> bb, vector<int>& bb_rev, Mat image);
void addBestError(vector<int>& error_matrix, vector<int> errors, vector<int> errors_rev);
void test(vector<cv::String> image_paths, vector<vector<int>> bounding_boxes, HOGDescriptor my_bird_hog, vector<int>& error_matrix, bool is_bird);
void getScores(vector<int> error_matrix, vector<vector<float>>& classifier_data);
void testClassifier(string data_path_pos, string data_path_neg, vector<vector<float>>& classifier_data, vector<int> pos_indices, vector<int> neg_indices);
