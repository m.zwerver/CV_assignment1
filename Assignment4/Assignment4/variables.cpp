#include "variables.h"

//found optimal values
float HEIGHT_PAR = 0.8;
int CELL_SIZE = 8;
float GRASS_PER = 0.6; //0.6
float POS_NEG_RATIO = 0.4; //# positive class images / #negative class images (range about 0.5 - 2.0)0.4
float HIT_THRESH = 0.7; //0.7
float DETECT_THRESH = 0.31;
float SVM_P = 0.1f;
float SVM_C = 0.01f;
int SVM_kernelType = 0;
int SVM_type = 103;

//SVM values
double WINSIG = -1;
float SVM_Coef = 0.0f;
int SVM_Degree = 0;
float SVM_Gamma = 0.0f;
float SVM_Nu = 0.6f;
