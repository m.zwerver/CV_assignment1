#pragma once

#include "stdafx.h"
#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "variables.h"
#include "globalFunctions.h"

using namespace std;
using namespace cv;

void getCutoutSizeAv(vector<vector<int>> bounding_boxes, vector<vector<int>>& bounding_boxes_adapted, int& min_width, int& max_height);
void getCutoutSize(vector<vector<int>> bounding_boxes, vector<vector<int>>& bounding_boxes_adapted, int& min_width, int& max_height);
void getCutouts(vector<vector<int>> bounding_boxes, vector<cv::String> image_paths, vector<Mat>& cutouts, Size& cutout_size, int cell_size);
void getStandardCutouts(vector<vector<int>> bounding_boxes, vector<cv::String> image_paths, vector<Mat>& cutouts, Size& cutout_size, int cell_size);
void drawCells(Mat image);
void getHogs(vector<Mat> cutouts, vector<vector<float>>& hog_descriptors, Size cutout_size, int cell_size_int);
void makeHog(string folder_path, Size& cutout_size, vector<vector<float>>& hog_descriptors_pos, int cell_size, vector<int> indices);
void getNegCutouts(vector<vector<int>> bounding_boxes_pos, vector<cv::String> pos_image_paths, vector<Mat>& cutouts, Size cutout_size);
void getNegCutoutsBird(vector<vector<int>> bounding_boxes_neg, vector<cv::String> neg_image_paths, vector<Mat>& cutouts_bird, Size cutout_size);
void makeNegHog(string folder_path_pos, string folder_path_neg, Size cutout_size, vector<vector<float>>& hog_descriptors_neg, int cell_size, vector<int> pos_indices, vector<int> neg_indices, vector<vector<float>>& classifier_data);
void getTrueNegativesCutouts(vector<Mat>& cutouts, Size& size);
void trainClassifier(string data_path_pos, string data_path_neg, vector<int> pos_indices, vector<int> neg_indices, vector<vector<float>>& classifier_data);
void saveSize(Size & size);
void getLabels(vector<Mat>& gradients, vector<int>& labels, vector<vector<float>>& hog_descriptors_pos, vector<vector<float>>& hog_descriptors_neg);
void train(vector<vector<float>>& hog_descriptors_pos, vector<vector<float>>& hog_descriptors_neg);
void vecToMat(vector<Mat>& gradients, Mat& traindata);
void CreateTrainSVMmodel(Mat & trainData, vector<int>& labels);