#pragma once

#include "stdafx.h"

//global variables/settings
static bool MIRROR = false;
static bool ADJ_CUT = false;
static bool STANDARD_CUT = false;
static bool ADP_THRESH = true;

//debug variables
static bool SHOW_IMG = false;
static bool SHOW_IMG_CUT = false;
static bool DEBUG = false;

static float NMS_OVERLAP = 0.5; //50% as is stated in the assignment
static int POS_IMG = 0;

extern float HEIGHT_PAR;
extern int  CELL_SIZE;
extern float GRASS_PER;
extern float POS_NEG_RATIO; //# positive class images / #negative class images (range about 0.5 - 2.0)

extern double WINSIG;

extern float HIT_THRESH;
extern float SVM_Coef;
extern int SVM_Degree;
extern float SVM_Gamma;
extern float SVM_Nu;
extern float SVM_P;
extern float SVM_C;
extern int SVM_type;
extern int SVM_kernelType;
extern float DETECT_THRESH;