#pragma once

#include "stdafx.h"
#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "variables.h"

//here all functions used by multiple stages
void getImageBBs(std::vector<int> image_ids, std::vector<std::vector<int>>& bounding_boxes);
void getImageIds(std::string folder_path, std::vector<int>& image_ids, std::vector<cv::String>& image_paths, std::vector<int> indices);