// Assignment4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "testModel.h"
#include "trainModel.h"
#include "globalFunctions.h"
#include "variables.h"

using namespace std;
using namespace cv;

//give the data location
void getDataPaths(vector<vector<string>>& data_paths)
{
	string data_path_right = "data/facing right/";
	string data_path_right_neg = "data/facing right neg/";
	string data_path_right_test = "data/facing right test/";

	vector<string> right_pose;
	right_pose.push_back(data_path_right);
	right_pose.push_back(data_path_right_neg);
	right_pose.push_back(data_path_right_test);

	data_paths.push_back(right_pose);
}

//generate k fold indices for the images
void generateIndices(string data_path, int k_folds, vector<vector<int>>& k_folds_indices)
{
	vector<cv::String> fn;
	glob(data_path + "*.jpg", fn); //gets all path names in folder
	for (int i = 0; i < fn.size(); i++)
	{
		k_folds_indices[i % k_folds].push_back(i);
	}
}

//group indices for each fold
void groupIndices(vector<vector<int>> k_folds_indices_pos, vector<vector<int>> k_folds_indices_neg, int ind, vector<int>& indices_pos, vector<int>& indices_neg)
{
	if (k_folds_indices_pos.size() == 1)
	{
		indices_pos = k_folds_indices_pos[0];
		indices_neg = k_folds_indices_neg[0];
		return;
	}

	for (int i = 0; i < k_folds_indices_pos.size(); i++)
	{
		if (i != ind)
		{
			indices_pos.insert(indices_pos.end(), k_folds_indices_pos[i].begin(), k_folds_indices_pos[i].end());
			indices_neg.insert(indices_neg.end(), k_folds_indices_neg[i].begin(), k_folds_indices_neg[i].end());
		}
	}
}

//calculate average score for each fold
void getAverageScores(vector<vector<float>> classifier_data, vector<float>& averages)
{
	for (int i = 0; i < classifier_data[0].size(); i++) // det_thresh, p, r, f
	{
		float sum = 0;
		for (int j = 0; j < classifier_data.size(); j++)
			sum += classifier_data[j][i];

		float average = sum / classifier_data.size();
		averages.push_back(average);
	}
}

//print fold results
void printResults(vector<float> averages) // pos, neg, bird, grass, (det_thresh, p, r, f, cell size) train, test
{
	cout << "#pos images: " << averages[0] << endl;
	cout << "#neg images: " << averages[1] << endl;
	cout << "#bird images: " << averages[2] << endl;
	cout << "#grass images: " << averages[3] << endl << endl;

	cout << "test: " << endl;
	cout << "average p score test: " << averages[4] << endl;
	cout << "average r score test: " << averages[5] << endl;
	cout << "average f score test: " << averages[6] << endl;

	cout << "train: " << endl;
	cout << "average p score train: " << averages[7] << endl;
	cout << "average r score train: " << averages[8] << endl;
	cout << "average f score train: " << averages[9] << endl << endl;
}

//Print optimal values found
void printOptimalValues(vector<string> names, vector<float> values)
{
	for (int i = 0; i < names.size(); i++)
	{
		cout << names[i] << ": " << values[i] << endl;
	}
	cout << endl;
}

//kfold test for cellsize and threshold
void kFoldsAllParSettings(int k_folds, int i, vector<vector<string>> data_paths)
{
	string data_path_pos = data_paths[i][0];
	string data_path_neg = data_paths[i][1];
	//string data_path_test = data_paths[i][2];

	vector<vector<int>> k_folds_indices_pos = vector<vector<int>>(k_folds);
	vector<vector<int>> k_folds_indices_neg = vector<vector<int>>(k_folds);
	vector<vector<float>> classifier_data;

	vector<string> names = { "cellSize", "threshold" };
	vector<float> values = { 0, 0 };

	generateIndices(data_path_pos, k_folds, k_folds_indices_pos);
	generateIndices(data_path_neg, k_folds, k_folds_indices_neg);

	vector<vector<int>> error_matrices = vector<vector<int>>(k_folds);
	vector<float> best_scores = { 0,0,0,0,0 };
	for (int c = 8; c <= 8; c = c * 2) //best 8
	{
		CELL_SIZE = c;
		for (int t = 1; t < 10; t++) //best 0.4
		{
			float threshold = t / 10.0;

			//k-folds cross validation to get best parameter settings
			for (int k = 0; k < k_folds; k++)
			{
				vector<int> indices_pos, indices_neg;
				groupIndices(k_folds_indices_pos, k_folds_indices_neg, k, indices_pos, indices_neg);
				trainClassifier(data_path_pos, data_path_neg, indices_pos, indices_neg, classifier_data); //1 pose
				testClassifier(data_path_pos, data_path_neg, classifier_data, k_folds_indices_pos[k], k_folds_indices_neg[k]);
				//				testClassifier(data_path_pos, data_path_neg, classifier_data, indices_pos, indices_neg);
			}
			vector<float> averages;
			getAverageScores(classifier_data, averages);
			printResults(averages);

			if (averages[6] > best_scores[6]) //f-score for test data
			{
				best_scores = averages;
				values = { (float)CELL_SIZE, threshold };
			}

			classifier_data.clear();
		}
	}
	printResults(best_scores);
	printOptimalValues(names, values);
}

//kfold test for SVM parameters (takes a very very long time)
void kFoldsSVMParSettings(int k_folds, int i, vector<vector<string>> data_paths)
{
	string data_path_pos = data_paths[i][0];
	string data_path_neg = data_paths[i][1];
	//string data_path_test = data_paths[i][2];

	vector<vector<int>> k_folds_indices_pos = vector<vector<int>>(k_folds);
	vector<vector<int>> k_folds_indices_neg = vector<vector<int>>(k_folds);
	vector<vector<float>> classifier_data;

	vector<string> names = { "SVM type", "SVM C value", "SVM kernel type", "SVM degree", "SVM Gamma", "SVM Coef0", "SVM nu", "SVM p" };
	vector<float> values = { 0, 0 };

	generateIndices(data_path_pos, k_folds, k_folds_indices_pos);
	generateIndices(data_path_neg, k_folds, k_folds_indices_neg);

	vector<vector<int>> error_matrices = vector<vector<int>>(k_folds);
	vector<float> best_scores = { 0,0,0,0,0 };
	for (int svmt = 103; svmt <= 104; svmt++)
	{
		SVM_type = svmt;
		for (float c = 0.01f; c < 3.0f; c += 0.01f)
		{
			SVM_C = c;
			for (int svmkt = 0; svmkt < 4; svmkt++)
			{
				SVM_kernelType = svmkt;

				for (int degree = 1; degree < 10 && svmkt == 1 || degree < 2 && svmkt != 1; degree++)
				{
					SVM_Degree == degree;

					for (float gamma = 0.1f; gamma < 1.0f && svmkt != 0 || gamma < 0.2f && svmkt == 0; gamma += 0.01f)
					{
						SVM_Gamma = gamma;

						for (float coef = 0.1f; coef < 2.0f && (svmkt == 1 || svmkt == 3) || coef < 0.2f && (svmkt != 1 && svmkt != 3); coef += 0.1f) // niet zeker
						{
							SVM_Coef = coef;

							for (float nu = 0.1f; nu < 1.0f && svmt == 104 || nu < 0.2f && svmt == 103; nu += 0.1f)
							{
								SVM_Nu = nu;

								for (float p = 0.1f; p < 1.0f && svmt != 104 || p < 0.2f && svmt != 103; p += 0.1f)
								{
									SVM_P = p;

									//k-folds cross validation to get best parameter settings
									for (int k = 0; k < k_folds; k++)
									{
										vector<int> indices_pos, indices_neg;
										groupIndices(k_folds_indices_pos, k_folds_indices_neg, k, indices_pos, indices_neg);
										trainClassifier(data_path_pos, data_path_neg, indices_pos, indices_neg, classifier_data); //1 pose
										testClassifier(data_path_pos, data_path_neg, classifier_data, k_folds_indices_pos[k], k_folds_indices_neg[k]);
										//				testClassifier(data_path_pos, data_path_neg, classifier_data, indices_pos, indices_neg);
									}
									vector<float> averages;
									getAverageScores(classifier_data, averages);
									printResults(averages);

									if (averages[6] > best_scores[6]) //f-score for test data
									{
										best_scores = averages;
										values = { (float)SVM_type, SVM_C, (float)SVM_kernelType, (float)SVM_Degree, SVM_Gamma, SVM_Coef, SVM_Nu, SVM_P };
									}

									classifier_data.clear();
								}
							}
						}
					}
				}
			}
		}
	}
	printResults(best_scores);
	printOptimalValues(names, values);
}

//kfold test one variable
void kFoldsOneParSetting(int k_folds, int i, vector<vector<string>> data_paths, int cell_size, float threshold)
{
	CELL_SIZE = cell_size;
	cout << CELL_SIZE << endl;
	string data_path_pos = data_paths[i][0];
	string data_path_neg = data_paths[i][1];
	//string data_path_test = data_paths[i][2];

	vector<vector<int>> k_folds_indices_pos = vector<vector<int>>(k_folds);
	vector<vector<int>> k_folds_indices_neg = vector<vector<int>>(k_folds);
	vector<vector<float>> classifier_data;

	vector<string> names = { "cellSize", "threshold" };
	vector<float> values = { (float)cell_size, threshold };

	DETECT_THRESH = threshold;
	generateIndices(data_path_pos, k_folds, k_folds_indices_pos);
	generateIndices(data_path_neg, k_folds, k_folds_indices_neg);

	vector<vector<int>> error_matrices = vector<vector<int>>(k_folds);

	//k-folds cross validation to get best parameter settings
	for (int k = 0; k < k_folds; k++)
	{
		vector<int> indices_pos, indices_neg;
		groupIndices(k_folds_indices_pos, k_folds_indices_neg, k, indices_pos, indices_neg);
		trainClassifier(data_path_pos, data_path_neg, indices_pos, indices_neg, classifier_data); //1 pose
		testClassifier(data_path_pos, data_path_neg, classifier_data, k_folds_indices_pos[k], k_folds_indices_neg[k]);
	}
	vector<float> averages;
	getAverageScores(classifier_data, averages);
	printResults(averages);
	printOptimalValues(names, values);
	classifier_data.clear();
}

//kfold test input data settings
void kFoldsCutoutSettings(int k_folds, int i, vector<vector<string>> data_paths)
{
	string data_path_pos = data_paths[i][0];
	string data_path_neg = data_paths[i][1];
	//string data_path_test = data_paths[i][2];

	vector<vector<int>> k_folds_indices_pos = vector<vector<int>>(k_folds);
	vector<vector<int>> k_folds_indices_neg = vector<vector<int>>(k_folds);
	vector<vector<float>> classifier_data;

	vector<string> names = { "adjusted cutouts", "pos neg ratio", "grass percentage" };
	vector<float> values = { 0, 0, 0 };

	generateIndices(data_path_pos, k_folds, k_folds_indices_pos);
	generateIndices(data_path_neg, k_folds, k_folds_indices_neg);

	vector<vector<int>> error_matrices = vector<vector<int>>(k_folds);
	vector<float> best_scores = { 0,0,0,0,0,0,0,0,0,0,0,0,0 };
	for (int i = 0; i <= 1; i++)
	{
		if (i == 0)
			ADJ_CUT = true;
		else
			ADJ_CUT = false;

		for (float n = 0.3; n <= 0.5f; n += 0.1) //best ?
		{
			POS_NEG_RATIO = n;
			cout << "n: " << n << endl;
			for (int g = 5; g <= 7; g++)
			{
				cout << "g: " << g << endl;

				GRASS_PER = g / 10.0;
				CELL_SIZE = 8;

				//k-folds cross validation to get best parameter settings
				for (int k = 0; k < k_folds; k++)
				{
					vector<int> indices_pos, indices_neg;
					groupIndices(k_folds_indices_pos, k_folds_indices_neg, k, indices_pos, indices_neg);
					trainClassifier(data_path_pos, data_path_neg, indices_pos, indices_neg, classifier_data); //1 pose
					testClassifier(data_path_pos, data_path_neg, classifier_data, k_folds_indices_pos[k], k_folds_indices_neg[k]);
					//					testClassifier(data_path_pos, data_path_neg, classifier_data, indices_pos, indices_neg);
				}
				vector<float> averages;
				getAverageScores(classifier_data, averages);
				printResults(averages);

				if (averages[6] > best_scores[6])
				{
					best_scores = averages;
					values = { (float)ADJ_CUT,(float)POS_NEG_RATIO, (float)GRASS_PER };
				}

				classifier_data.clear();
			}
		}
	}
	printResults(best_scores);
	printOptimalValues(names, values);
}

//kfolds detect best thresshold for multiscale detect
void kFoldsPR(int k_folds, int i, vector<vector<string>> data_paths)
{
	string data_path_pos = data_paths[i][0];
	string data_path_neg = data_paths[i][1];
	//string data_path_test = data_paths[i][2];

	vector<vector<int>> k_folds_indices_pos = vector<vector<int>>(k_folds);
	vector<vector<int>> k_folds_indices_neg = vector<vector<int>>(k_folds);
	vector<vector<float>> classifier_data;

	vector<string> names = { "cellSize", "threshold" };
	vector<float> values = { 0, 0 };

	generateIndices(data_path_pos, k_folds, k_folds_indices_pos);
	generateIndices(data_path_neg, k_folds, k_folds_indices_neg);

	vector<vector<int>> error_matrices = vector<vector<int>>(k_folds);
	vector<float> best_scores = { 0,0,0,0,0,0,0,0,0,0 };

	for (int t = 1; t < 10; t++)
	{
		float threshold = t / 10.0;
		DETECT_THRESH = threshold;
		//k-folds cross validation to get best parameter settings
		for (int k = 0; k < k_folds; k++)
		{
			vector<int> indices_pos, indices_neg;
			groupIndices(k_folds_indices_pos, k_folds_indices_neg, k, indices_pos, indices_neg);
			trainClassifier(data_path_pos, data_path_neg, indices_pos, indices_neg, classifier_data); //1 pose
			testClassifier(data_path_pos, data_path_neg, classifier_data, k_folds_indices_pos[k], k_folds_indices_neg[k]);
			//				testClassifier(data_path_pos, data_path_neg, classifier_data, indices_pos, indices_neg);
		}
		vector<float> averages;
		getAverageScores(classifier_data, averages);
		cout << "thres: " << threshold << endl;
		printResults(averages);

		if (averages[6] > best_scores[6]) //f-score for test data
		{
			best_scores = averages;
			values = { (float)CELL_SIZE, threshold };
		}

		classifier_data.clear();
	}
	//printResults(best_scores);
	//printOptimalValues(names, values);
}

//main function
int main()
{
	vector<vector<string>> data_paths;
	getDataPaths(data_paths);

	for (int i = 0; i < data_paths.size(); i++)
	{
		//kFoldsPR(5, i, data_paths);
		//kFoldsAllParSettings(5, i, data_paths);
		//kFoldsCutoutSettings(5, i, data_paths);
		kFoldsOneParSetting(5, i, data_paths, 8, 0.31);
		//kFoldsSVMParSettings(5, i, data_paths);
	}
	cin.ignore();
	return 0;
}

