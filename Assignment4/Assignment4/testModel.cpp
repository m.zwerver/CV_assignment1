#include "testModel.h"

using namespace std;
using namespace cv;

//Generate a Hog descriptor with a SVM
void testClassifier(string data_path_pos, string data_path_neg, vector<vector<float>>& classifier_data, vector<int> pos_indices, vector<int> neg_indices)
{
	//positive test cases
	vector<cv::String> image_paths_pos;
	vector<int> image_ids_pos;
	getImageIds(data_path_pos, image_ids_pos, image_paths_pos, pos_indices);

	vector<vector<int>> bounding_boxes_pos;
	getImageBBs(image_ids_pos, bounding_boxes_pos);

	//negative test cases
	vector<cv::String> image_paths_neg;
	vector<int> image_ids_neg;
	getImageIds(data_path_neg, image_ids_neg, image_paths_neg, neg_indices);

	vector<vector<int>> bounding_boxes_neg;
	getImageBBs(image_ids_neg, bounding_boxes_neg);

	//setup hogdescriptor
	Size size;
	loadSize(size);
	Size cell_size = Size(CELL_SIZE, CELL_SIZE);
	Size block_size = Size(CELL_SIZE * 2, CELL_SIZE * 2);
	HOGDescriptor my_bird_hog;
	my_bird_hog.winSize = size;
	my_bird_hog.blockSize = block_size;
	my_bird_hog.blockStride = cell_size;
	my_bird_hog.cellSize = cell_size;
	my_bird_hog.winSigma = WINSIG;

	Ptr<ml::SVM> svm;
	svm = ml::StatModel::load<ml::SVM>("my_birdo.yml");
	vector<float> detector;
	get_svm_detector(svm, detector);
	my_bird_hog.setSVMDetector(detector);

	vector<int> error_matrix = { 0, 0, 0 }; //true positive, false positive, false negative
	test(image_paths_pos, bounding_boxes_pos, my_bird_hog, error_matrix, true);
	test(image_paths_neg, bounding_boxes_neg, my_bird_hog, error_matrix, false);

	getScores(error_matrix, classifier_data);
}

//Load window Size
void loadSize(Size & size)
{
	string line;
	ifstream myfile("settings.txt");
	if (myfile.is_open())
	{
		int counter = 0;
		while (getline(myfile, line))
		{
			if (counter == 0)
				size.width = stoi(line);
			else
				size.height = stoi(line);
			counter++;
		}
		myfile.close();
	}
}

//Create a Vector of floats from the SVM
void get_svm_detector(Ptr<ml::SVM>& svm, vector< float > & hog_detector)
{
	// get the support vectors
	Mat sv = svm->getSupportVectors();
	const int sv_total = sv.rows;
	// get the decision function
	Mat alpha, svidx;
	double rho = svm->getDecisionFunction(0, alpha, svidx);

	hog_detector.clear();

	hog_detector.resize(sv.cols + 1);
	memcpy(&hog_detector[0], sv.ptr(), sv.cols * sizeof(hog_detector[0]));
	hog_detector[sv.cols] = (float)-rho;
}

//Draw the found bounding boxes. Color corresponds to color
void drawLocations(Mat& img, const vector<Rect>& locations, vector<double> found_weights)
{
	if (!locations.empty())
	{
		vector<Rect>::const_iterator loc = locations.begin();
		vector<double>::const_iterator weight = found_weights.begin();

		vector< Rect >::const_iterator end = locations.end();
		for (; loc != end; ++loc, ++weight)
		{
			CvScalar color = CvScalar(0, 255 * *weight, 0);
			rectangle(img, *loc, color, 2);
		}
	}
}

//uses bubble sort to order from highest to lowest score
void orderBBs(vector<Rect>& locations, vector<double>& found_weights)
{
	double temp = 0;
	Rect temp_r;
	for (int i = 0; i < found_weights.size(); i++)
	{
		for (int j = 0; j < found_weights.size() - 1; j++)
		{
			if (found_weights.at(j) < found_weights.at(j + 1))
			{
				//switch weights
				temp = found_weights.at(j + 1);
				found_weights.at(j + 1) = found_weights.at(j);
				found_weights.at(j) = temp;

				//switch bbs
				temp_r = locations.at(j + 1);
				locations.at(j + 1) = locations.at(j);
				locations.at(j) = temp_r;
			}
		}
	}
}

//use non maximum suppression to remove bbs below a certain threshold 
void nonMaxSuppression(vector<Rect>& locations, vector<double>& found_weights, int start)
{
	vector<int> ind_remove = vector<int>();

	if (locations.size() == 0 || start + 1 > locations.size() - 1)
		return;

	for (int j = start + 1; j < locations.size(); j++)
	{
		Rect r_high = locations.at(start);
		Rect r_low = locations.at(j);

		Rect overlap = r_high & r_low;
		Rect intersection = r_high | r_low;
		float i_o_u = (float)overlap.area() / intersection.area();
		if (i_o_u > NMS_OVERLAP)
			ind_remove.push_back(j);
	}

	//remove indexes
	for (int i = ind_remove.size() - 1; i >= 0; i--)
	{
		locations.erase(locations.begin() + ind_remove.at(i));
		found_weights.erase(found_weights.begin() + ind_remove.at(i));
	}
	nonMaxSuppression(locations, found_weights, start + 1);
}

//in theory multiple true positives are possible, if none is found, a false negative is added
void getErrors(vector<Rect> locations, Rect correct_bb, vector<int>& error_matrix, bool is_bird)//true positive, false positive, false negative
{
	if (is_bird) //image has a positive class bb
	{
		bool get_true_pos = false;
		for (int i = 0; i < locations.size(); i++)
		{
			Rect r = locations.at(i);
			Rect overlap = r & correct_bb;
			Rect intersection = r | correct_bb;
			float i_o_u = (float)overlap.area() / intersection.area();

			if (i_o_u > HIT_THRESH)
			{
				error_matrix.at(0) += 1; //true positive
				get_true_pos = true;
			}
			else
				error_matrix.at(1) += 1; //false positive
		}

		if (!get_true_pos)
			error_matrix.at(2) += 1; //false negative
	}

	else //image does not have a positive class bb, every found bb is a false positive
		error_matrix.at(1) += locations.size(); //false positive

	if (DEBUG && SHOW_IMG)
		cout << "true pos: " << error_matrix.at(0) << ", false pos: " << error_matrix.at(1) << ", false neg: " << error_matrix.at(2) << endl;
}

//Function to mirror the boundingbox when image gets mirrored
void mirrorBoundingBox(vector<int> bb, vector<int>& bb_rev, Mat image)
{
	//bb: x, y, width, height
	bb_rev.push_back(image.cols - (bb[0] + bb[2])); //new x = image width - (x + bb width)
	bb_rev.push_back(bb[1]);
	bb_rev.push_back(bb[2]);
	bb_rev.push_back(bb[3]);
}

//Function for checking which image gives the best results, the original one or the mirrored one
void addBestError(vector<int>& error_matrix, vector<int> errors, vector<int> errors_rev) //true positive, false positive, false negative
{
	if (errors[0] > errors_rev[0] || (errors[1] + errors[2]) < (errors_rev[1] + errors_rev[2]))
	{
		error_matrix[0] += errors[0];
		error_matrix[1] += errors[1];
		error_matrix[2] += errors[2];
		if (DEBUG && SHOW_IMG)
			cout << "\ best" << endl << endl;
	}
	else
	{
		error_matrix[0] += errors_rev[0];
		error_matrix[1] += errors_rev[1];
		error_matrix[2] += errors_rev[2];
		if (DEBUG && SHOW_IMG)
			cout << "reversed image best" << endl << endl;
	}
}

//test all images with the Hog descriptor and clean the results
void test(vector<cv::String> image_paths, vector<vector<int>> bounding_boxes, HOGDescriptor my_bird_hog, vector<int>& error_matrix, bool is_bird)
{
	vector<Rect> locations = vector<Rect>();

	for (int i = 0; i < image_paths.size(); i++)
	{
		vector<int> bb = bounding_boxes[i];
		Rect rec = Rect(bb[0], bb[1], bb[2], bb[3]);
		locations.clear();

		Mat test_image = imread(image_paths[i]);
		Mat rev_test_image;
		flip(test_image, rev_test_image, 1);
		vector<double> found_weights = vector<double>(), found_weights_rev;

		//regular image
		my_bird_hog.detectMultiScale(test_image, locations, found_weights, DETECT_THRESH);
		orderBBs(locations, found_weights);
		nonMaxSuppression(locations, found_weights);

		vector<int> errors = { 0,0,0 };
		getErrors(locations, rec, errors, is_bird);
		drawLocations(test_image, locations, found_weights);

		CvScalar color = CvScalar(0, 255, 0); //green
		rectangle(test_image, rec, color);
		locations.clear();

		//mirrored image
		if (MIRROR)
		{
			my_bird_hog.detectMultiScale(rev_test_image, locations, found_weights_rev);
			orderBBs(locations, found_weights_rev);
			nonMaxSuppression(locations, found_weights_rev);

			vector<int> bb_rev;
			mirrorBoundingBox(bb, bb_rev, test_image);
			Rect rec_rev = Rect(bb_rev[0], bb_rev[1], bb_rev[2], bb_rev[3]);
			vector<int> errors_rev = { 0,0,0 };
			getErrors(locations, rec, errors_rev, is_bird);
			drawLocations(rev_test_image, locations, found_weights_rev);
			locations.clear();

			addBestError(error_matrix, errors, errors_rev);
			rectangle(rev_test_image, rec_rev, color);
		}
		else
			addBestError(error_matrix, errors, { -1, 100, 100 });




		if (SHOW_IMG)
		{
			imshow("test regular", test_image);
			imshow("test reversed", rev_test_image);
			waitKey();
		}
	}
}

//generate the precision, recall and F-Scores
void getScores(vector<int> error_matrix, vector<vector<float>>& classifier_data)
{
	float tp = error_matrix.at(0);
	float fp = error_matrix.at(1);
	float fn = error_matrix.at(2);

	float p = tp / (tp + fp);
	if (tp + fp == 0)
		p = 0;

	float r = tp / (tp + fn);
	if (tp + fn == 0)
		r = 0;

	float f = 2 * p*r / (p + r);
	if (p == 0 && r == 0)
		f = 0;

	//cout << "p: " << p << ", r: " << r << ", f: " << f << endl;
	vector<float> data = { p, r, f };
	classifier_data[classifier_data.size() - 1].insert(classifier_data[classifier_data.size() - 1].end(), data.begin(), data.end());
}
