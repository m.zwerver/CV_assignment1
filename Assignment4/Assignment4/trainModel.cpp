#include "trainModel.h"

using namespace std;
using namespace cv;

//Generates an average sized bounding box from all the bounding boxes
void getCutoutSizeAv(vector<vector<int>> bounding_boxes, vector<vector<int>>& bounding_boxes_adapted, int& min_width, int& max_height)
{
	float sum_width = 0;
	float sum_height = 0;

	for (int i = 0; i < bounding_boxes.size(); i++)
	{
		sum_width += bounding_boxes[i][2];
		sum_height += bounding_boxes[i][3];
	}
	float average_ratio = sum_width / sum_height;

	for (int i = 0; i < bounding_boxes_adapted.size(); i++)
	{
		float adjusted_width = bounding_boxes_adapted[i][3] * average_ratio;
		if (adjusted_width > bounding_boxes_adapted[i][2]) //extra width is needed to meet ratio
		{
			float difference = adjusted_width - bounding_boxes_adapted[i][2];
			bounding_boxes_adapted[i][0] -= difference / 2;
			bounding_boxes_adapted[i][2] = adjusted_width;
		}
		else
		{
			float adapted_height = bounding_boxes_adapted[i][2] / average_ratio;
			float difference = adapted_height - bounding_boxes_adapted[i][3];
			bounding_boxes_adapted[i][1] -= difference / 2;
			bounding_boxes_adapted[i][3] += difference;
		}
	}

	min_width = 10000; //might be bigger possible numbers
					   //find min width
	for (int i = 0; i < bounding_boxes_adapted.size(); i++)
	{
		float box_width = bounding_boxes_adapted[i][2];
		if (box_width < min_width)
			min_width = box_width;
	}
	max_height = min_width / average_ratio;
}

//Generates bounding boxes according to largest height bounding box
void getCutoutSize(vector<vector<int>> bounding_boxes, vector<vector<int>>& bounding_boxes_adapted, int& min_width, int& max_height)
{
	float max_adapted_height = 0;
	min_width = 10000; //might be bigger possible numbers
					   //find min width
	for (int i = 0; i < bounding_boxes.size(); i++)
	{
		float box_width = bounding_boxes[i][2];
		if (box_width < min_width)
			min_width = box_width;
	}

	//cout << "max width : " << max_width << endl;

	for (int i = 0; i < bounding_boxes.size(); i++)
	{
		//float new_box_height = (max_width / bounding_boxes[i][2]) * bounding_boxes[i][3];
		float new_box_height = (min_width * bounding_boxes[i][3]) / bounding_boxes[i][2];


		if (new_box_height > max_adapted_height)
			max_adapted_height = new_box_height;
	}
	//cout << "max: " << max_adapted_height << endl; //should be 338 or more
	//cout << "height parameter is now set to: " << HEIGHT_PAR << endl;
	max_adapted_height = max_adapted_height * HEIGHT_PAR;
	float ratio = min_width / max_adapted_height;
	//cout << "max adapted height: " << max_adapted_height << endl;

	for (int i = 0; i < bounding_boxes_adapted.size(); i++)
	{
		float adapted_height = bounding_boxes_adapted[i][2] / ratio; //adapted height always larger than original height?
		float difference = adapted_height - bounding_boxes_adapted[i][3];
		//cout << "ad height: " << adapted_height << endl;;
		//cout << "dif: " << difference << endl;
		bounding_boxes_adapted[i][1] -= difference / 2;
		bounding_boxes_adapted[i][3] += difference;
	}
	max_height = max_adapted_height;
}

//resizes the cutouts according to the new bounding boxes
void getCutouts(vector<vector<int>> bounding_boxes, vector<cv::String> image_paths, vector<Mat>& cutouts, Size& cutout_size, int cell_size)
{
	int success = 0;
	vector<vector<int>> bounding_boxes_adapted = bounding_boxes;
	int min_width = 0;
	int max_height = 0;
	//float ratio;
	if (ADJ_CUT)
		getCutoutSizeAv(bounding_boxes, bounding_boxes_adapted, min_width, max_height);

	else
		getCutoutSize(bounding_boxes, bounding_boxes_adapted, min_width, max_height);

	float overlap_sum = 0;
	int img_amount = 0;

	vector<Mat> images;
	for (size_t i = 0; i < image_paths.size(); i++)
	{
		Mat image = imread(image_paths[i]);
		images.push_back(image);
		if (SHOW_IMG_CUT)
		{
			imshow("original", image);
			waitKey();
		}

		vector<int> c = bounding_boxes[i];
		Mat cutout = Mat(image, cv::Rect(c[0], c[1], c[2] - 1, c[3] - 1));

		if (SHOW_IMG_CUT)
		{
			imshow("cutout", cutout);
			waitKey();
		}

		vector<int> c2 = bounding_boxes_adapted[i];
		vector<Mat> hog_images;
		if (!(c2[1] < 0) && !(c2[1] + c2[3] > image.rows) && (c2[3] > bounding_boxes[i][3]))
		{
			Mat cutout2 = Mat(image, cv::Rect(c2[0], c2[1], c2[2], c2[3]));

			if (SHOW_IMG_CUT)
			{
				imshow("adapted cutout", cutout2);
				waitKey();
			}
			Mat resized_image;
			Size s;

			Rect r1 = Rect(c[0], c[1], c[2], c[3]);
			Rect r2 = Rect(c2[0], c2[1], c2[2], c2[3]);

			Rect overlap = r1 & r2;
			Rect intersection = r1 | r2;
			float i_o_u = (float)overlap.area() / intersection.area();
			overlap_sum += i_o_u;
			img_amount++;

			//image will be resized to this width and height which are the largest multiples of the cell size
			s.height = (max_height / cell_size) * cell_size;
			s.width = (min_width / cell_size) * cell_size;
			cutout_size = Size(s.width, s.height);
			float ratio = ((double)min_width) / c2[2];
			//int new_height = c2[3] * ratio;
			//cout << "height: " << new_height;
			resize(cutout2, resized_image, s);

			//cout << "adapted width, height: " << cutout2.cols << ", " << cutout2.rows << endl;
			//cout << "resized width, height: " << resized_image.cols << ", " << resized_image.rows << endl;
			cutouts.push_back(resized_image);
			if (SHOW_IMG_CUT)
			{
				imshow("resized adapted cutout", resized_image);
				waitKey();
			}

			success++;
		}
		else
			;//cout << "error: not enough image to increase height!" << endl;
	}
	if (DEBUG)
		cout << "successfully got adapted cutouts for " << success << " out of " << image_paths.size() << " images" << endl;
	POS_IMG = success;
	//cout << "bb overlap with box of size: " << overlap_sum / img_amount << endl;
	if (ADP_THRESH)
		HIT_THRESH = 0.7 * (overlap_sum / img_amount);
}

//test function for resizing cutouts to standard size
void getStandardCutouts(vector<vector<int>> bounding_boxes, vector<cv::String> image_paths, vector<Mat>& cutouts, Size& cutout_size, int cell_size)
{
	int max_height = 90;
	int min_width = 120;
	Size s;

	//image will be resized to this width and height which are the largest multiples of the cell size
	s.height = (max_height / cell_size) * cell_size;
	s.width = (min_width / cell_size) * cell_size;
	cutout_size = s;
	vector<Mat> images;
	for (size_t i = 0; i < image_paths.size(); i++)
	{
		Mat image = imread(image_paths[i]);
		images.push_back(image);
		if (SHOW_IMG_CUT)
		{
			imshow("original", image);
			waitKey();
		}

		vector<int> c = bounding_boxes[i];
		Mat cutout = Mat(image, cv::Rect(c[0], c[1], c[2] - 1, c[3] - 1));

		if (SHOW_IMG_CUT)
		{
			imshow("cutout", cutout);
			waitKey();
		}

		Mat resizeImage;
		resize(cutout, resizeImage, s);

		if (SHOW_IMG_CUT)
		{
			imshow("resized", resizeImage);
			waitKey();
		}

		cutouts.push_back(resizeImage);
	}
	POS_IMG = cutouts.size();
}

//test function for drawing on the cell sizes
void drawCells(Mat image)
{
	CvScalar color = CvScalar(255, 0, 0);
	for (int n = 64; n >= 4; n = n / 2)
	{
		for (int i = 0; i < image.rows / n; i++)
		{
			for (int j = 0; j < image.cols / n; j++)
			{
				Rect r = Rect(j * n, i * n, n, n);
				rectangle(image, r, color);
			}
		}
		cout << n << endl;

		imshow(to_string(n), image);
		//waitKey();
	}
}

//computes the HOG descriptors from the cutouts
void getHogs(vector<Mat> cutouts, vector<vector<float>>& hog_descriptors, Size cutout_size, int cell_size_int)
{
	for (int i = 0; i < cutouts.size(); i++)
	{
		Mat image = cutouts[i];
		//drawCells(image);
		image.convertTo(image, CV_32F, 1 / 255.0); //normalize colors

		Mat gradient_x, gradient_y;

		Sobel(image, gradient_x, CV_32F, 1, 0, 1);
		Sobel(image, gradient_y, CV_32F, 0, 1, 1);

		//imshow("gradient x", gradient_x);
		//waitKey();

		//imshow("gradient y", gradient_y);
		//waitKey();

		//cout << gradient_x.rows << ", " << gradient_x.cols << endl;

		Mat magnitude, angle;
		cartToPolar(gradient_x, gradient_y, magnitude, angle, 1);

		image.convertTo(image, CV_8UC3);

		//HOGDescriptor(windows size, block size (pixels), ?block stride (multiple of cell size), cell size, #bins)
		Size cell_size = Size(cell_size_int, cell_size_int);
		Size block_size = Size(cell_size_int * 2, cell_size_int * 2);
		HOGDescriptor hog = HOGDescriptor(cutout_size, block_size, cell_size, cell_size, 9);
		hog.winSigma = WINSIG;
		vector<float> descriptors;

		//compute(image, descriptors, ?window stride (multiple of block size), padding)
		hog.compute(image, descriptors, block_size, Size(0, 0));
		//descriptor has 17100 values, 25x19 blocks x 4 cells per block x 9 bins per cell?

		hog_descriptors.push_back(descriptors);
	}
}

//Collection function which calls everything to get the hog descriptors
void makeHog(string folder_path, Size& cutout_size, vector<vector<float>>& hog_descriptors_pos, int cell_size, vector<int> indices)
{
	vector<cv::String> image_paths;
	vector<int> image_ids;
	getImageIds(folder_path, image_ids, image_paths, indices);

	vector<vector<int>> bounding_boxes;
	getImageBBs(image_ids, bounding_boxes);

	vector<Mat> cutouts;
	if (STANDARD_CUT)
		getStandardCutouts(bounding_boxes, image_paths, cutouts, cutout_size, cell_size);
	else
		getCutouts(bounding_boxes, image_paths, cutouts, cutout_size, cell_size);


	getHogs(cutouts, hog_descriptors_pos, cutout_size, cell_size);
}

//Generates negative cutouts from positive images
void getNegCutouts(vector<vector<int>> bounding_boxes_pos, vector<cv::String> pos_image_paths, vector<Mat>& cutouts, Size cutout_size)
{
	float ratio = (float)cutout_size.width / cutout_size.height;
	for (int i = 0; i < bounding_boxes_pos.size(); i++)
	{
		vector<int> bounding_box = bounding_boxes_pos[i]; //x, y, width, height
		int x = bounding_box[0];
		int y = bounding_box[1];
		int box_width = bounding_box[2];
		int box_height = bounding_box[3];

		Mat image = imread(pos_image_paths[i]);
		int image_width = image.cols;
		int image_height = image.rows;

		int spare_room_up = y;
		int spare_room_down = image_height - (y + box_height);

		int spare_room_left = x;
		int spare_room_right = image_width - (x + box_width);

		//cout << spare_room_up << ", " << spare_room_down << endl;
		//cout << spare_room_left << ", " << spare_room_right << endl;

		bool up;   //down -> false
		bool left; //right -> false

		int spare_room_ver;
		if (spare_room_up > spare_room_down)
		{
			up = true;
			spare_room_ver = spare_room_up;
		}
		else
		{
			up = false;
			spare_room_ver = spare_room_down;
		}

		int spare_room_hor;
		if (spare_room_left > spare_room_right)
		{
			left = true;
			spare_room_hor = spare_room_left;

		}
		else
		{
			left = false;
			spare_room_hor = spare_room_right;
		}

		int pos_width = ratio * spare_room_ver; //width of the cutout above or below the original cutout (same ratio)

		Mat cutout;
		vector<int> rect;
		if (pos_width > spare_room_hor) //negative cutout is above or below original cutout
		{
			int width = ratio * spare_room_ver;
			if (width > image_width) //in some cases (when bird is high) new width will excede image width
			{
				width = image_width;
				spare_room_ver = image_width / ratio;
			}

			rect.push_back(image_width / 2 - width / 2);//

			if (up)						//above
				rect.push_back(0);

			else
				rect.push_back(y + box_height);

			rect.push_back(width);
			rect.push_back(spare_room_ver);
		}
		else
		{
			int height = spare_room_hor / ratio;
			if (height > image_height) //in some cases (when bird is small) new height will excede image height
			{
				height = image_height;
				spare_room_hor = image_height * ratio;
			}

			if (left)						//negative cutout is left of original cutout
				rect.push_back(0);

			else
				rect.push_back(x + box_width);

			rect.push_back(image_height / 2 - height / 2);//
			rect.push_back(spare_room_hor);
			rect.push_back(height);
		}
		//cout << up << ", " << left << endl;
		cutout = Mat(image, cv::Rect(rect[0], rect[1], rect[2], rect[3]));
		//imshow("image", image);
		//imshow("cutout", cutout);//not resized yet
		//waitKey();

		resize(cutout, cutout, cutout_size);
		//imshow("cutout", cutout);//resized
		//waitKey();
		cutouts.push_back(cutout);
	}

	if (DEBUG)
		cout << cutouts.size() << " grass cutouts gathered" << endl;
}

//Generates negative bird cutouts/ the species we do not want to classify
void getNegCutoutsBird(vector<vector<int>> bounding_boxes_neg, vector<cv::String> neg_image_paths, vector<Mat>& cutouts_bird, Size cutout_size)
{
	float train_ratio = (float)cutout_size.width / cutout_size.height;

	for (int i = 0; i < bounding_boxes_neg.size(); i++)
	{
		Mat image = imread(neg_image_paths[i]);
		vector<int> bounding_box = bounding_boxes_neg[i];

		Mat original_cutout = Mat(image, Rect(bounding_box[0], bounding_box[1], bounding_box[2], bounding_box[3]));
		//imshow("original cutout", original_cutout);


		float bb_ratio = (float)bounding_box[2] / bounding_box[3];

		//bb ratio is smaller -> expand width of bb
		if (bb_ratio < train_ratio)
		{
			int new_width = bounding_box[3] * train_ratio;
			int width_dif = new_width - bounding_box[2]; //if this is negative, something went wrong
			bounding_box[2] = new_width;
			bounding_box[0] = bounding_box[0] - (width_dif / 2.0);//adapt x to new width to center bird in bb

			if ((bounding_box[0] + new_width > image.cols) || bounding_box[0] < 0) //not possible to extend bb
				continue;
		}

		//bb ratio is bigger -> expand height of bb
		else
		{
			int new_height = bounding_box[2] / train_ratio;
			int height_dif = new_height - bounding_box[3]; //if this is negative, something went wrong
			bounding_box[3] = new_height;
			bounding_box[1] = bounding_box[1] - (height_dif / 2.0);//adapt y to new height to center bird in bb

			if ((bounding_box[1] + new_height > image.rows) || bounding_box[1] < 0) //not possible to extend bb
				continue;
		}

		//cout << bounding_box[0] << ", " << bounding_box[1] << ", " << bounding_box[2] << ", " << bounding_box[3] << endl;
		Mat adapted_cutout = Mat(image, Rect(bounding_box[0], bounding_box[1], bounding_box[2], bounding_box[3]));
		//imshow("adapted cutout", adapted_cutout);

		resize(adapted_cutout, adapted_cutout, cutout_size);
		//imshow("adapted cutout resize", adapted_cutout);
		//waitKey();

		cutouts_bird.push_back(adapted_cutout);
	}
	if (DEBUG)
		cout << "successfully got negative class bird cutouts from " << cutouts_bird.size() << " out of " << bounding_boxes_neg.size() << " images" << endl;
}

//Function which generates HOG descriptors for all negative samples
void makeNegHog(string folder_path_pos, string folder_path_neg, Size cutout_size, vector<vector<float>>& hog_descriptors_neg, int cell_size, vector<int> pos_indices, vector<int> neg_indices, vector<vector<float>>& classifier_data)
{
	vector<Mat> cutouts;

	//Other bird species
#pragma region other species

	//for the negative images, we need cutouts (of the birds) with the same size bb
	vector<cv::String> neg_image_paths;
	vector<int> neg_image_ids;
	getImageIds(folder_path_neg, neg_image_ids, neg_image_paths, neg_indices);

	vector<vector<int>> bounding_boxes_neg;
	getImageBBs(neg_image_ids, bounding_boxes_neg);

	vector<Mat> cutouts_bird;
	getNegCutoutsBird(bounding_boxes_neg, neg_image_paths, cutouts_bird, cutout_size);

#pragma endregion

	//cutouts from positive images
#pragma region cutout of positive images
	//for the positive images, we need random cutouts of the same size
	vector<cv::String> pos_image_paths;
	vector<int> pos_image_ids;
	getImageIds(folder_path_pos, pos_image_ids, pos_image_paths, pos_indices);

	vector<vector<int>> bounding_boxes_pos;
	getImageBBs(pos_image_ids, bounding_boxes_pos);

	vector<Mat> cutouts_grass;
	getNegCutouts(bounding_boxes_pos, pos_image_paths, cutouts_grass, cutout_size);

#pragma endregion

#pragma region shorten cutouts

	float neg_images = POS_IMG / POS_NEG_RATIO;
	if (neg_images > cutouts_bird.size() + cutouts_grass.size())
	{
		neg_images = cutouts_bird.size() + cutouts_grass.size();
		if (DEBUG)
			cout << "not enough negative images to meet pos/neg ratio!" << endl;
	}

	int bird_amount = neg_images - (neg_images * GRASS_PER);
	if (bird_amount > cutouts_bird.size())
	{
		bird_amount = cutouts_bird.size();
		if (DEBUG)
			cout << "not enough negative bird images to meet bird/grass ratio!" << endl;
	}

	int grass_amount = neg_images - bird_amount;

	if (grass_amount > cutouts_grass.size())
		grass_amount = cutouts_grass.size();

	vector<Mat> cutouts_bird_short(cutouts_bird.begin(), cutouts_bird.begin() + bird_amount);
	cutouts.insert(cutouts.end(), cutouts_bird_short.begin(), cutouts_bird_short.end());
	vector<Mat> cutouts_grass_short(cutouts_grass.begin(), cutouts_grass.begin() + grass_amount);
	cutouts.insert(cutouts.end(), cutouts_grass_short.begin(), cutouts_grass_short.end());

#pragma endregion

	if (DEBUG)
		cout << "pos images: " << POS_IMG << ", neg images: " << bird_amount + grass_amount << ", neg bird images: " << bird_amount << ", neg grass images: " << grass_amount << endl;
	vector<float> data = { (float)POS_IMG, (float)(bird_amount + grass_amount), (float)bird_amount, (float)grass_amount };
	classifier_data.push_back(data);
#pragma region True negative pictures

	vector<Mat> tnCutouts;
	getTrueNegativesCutouts(tnCutouts, cutout_size);

	cutouts.insert(cutouts.end(), tnCutouts.begin(), tnCutouts.end());
#pragma endregion


	getHogs(cutouts, hog_descriptors_neg, cutout_size, cell_size);

}

//Function which rescales branch or grass images to correct size
void getTrueNegativesCutouts(vector<Mat>& cutouts, Size& size)
{
	string folder_path = "data/true negative/";
	vector<cv::String> fn;
	glob(folder_path + "*.jpg", fn); //gets all path names in folder
	vector<cv::String> image_paths = fn;

	for (int i = 0; i < fn.size(); i++)
	{
		Mat whole = imread(image_paths[i]);
		/*
		//part of picture
		for (int i = 0; i < (int)whole.cols / size.width; i++)
		{
		for (int j = 0; j < (int)whole.rows / size.height; j++)
		{
		Mat piece;
		piece = whole(Rect(i*size.width, j*size.height, size.width, size.height));
		cutouts.push_back(piece);
		}
		}*/

		//or the whole picture
		Mat resizeTest;
		resize(whole, resizeTest, size);
		cutouts.push_back(resizeTest);
	}

}

//Function which collects the HOG's and calls a train method on them
void trainClassifier(string data_path_pos, string data_path_neg, vector<int> pos_indices, vector<int> neg_indices, vector<vector<float>>& classifier_data)
{
	Size cutout_size;

	vector<vector<float>> hog_descriptors_pos;
	int cell_size = CELL_SIZE;
	makeHog(data_path_pos, cutout_size, hog_descriptors_pos, cell_size, pos_indices);
	saveSize(cutout_size);
	vector<vector<float>> hog_descriptors_neg;
	makeNegHog(data_path_pos, data_path_neg, cutout_size, hog_descriptors_neg, cell_size, pos_indices, neg_indices, classifier_data);

	train(hog_descriptors_pos, hog_descriptors_neg);
}

//saveSize to file
void saveSize(Size & size)
{
	ofstream myfile;
	myfile.open("settings.txt");
	myfile << size.width;
	myfile << "\n";
	myfile << size.height;
	myfile.close();
}

//Labels HOG's as negative or possitive and return the labels and one vector with all HOG descriptors
void getLabels(vector<Mat>& gradients, vector<int>& labels, vector<vector<float>>& hog_descriptors_pos, vector<vector<float>>& hog_descriptors_neg)
{
	for (int i = 0; i < hog_descriptors_pos.size(); i++)
	{
		gradients.push_back(Mat(hog_descriptors_pos[i]).clone());
		labels.push_back(1);
	}

	for (int i = 0; i < hog_descriptors_neg.size(); i++)
	{
		gradients.push_back(Mat(hog_descriptors_neg[i]).clone());
		labels.push_back(-1);
	}
}

//Function to create the train model
void train(vector<vector<float>>& hog_descriptors_pos, vector<vector<float>>& hog_descriptors_neg)
{
	vector<Mat> gradients;
	vector<int> labels;
	Mat trainData;
	//generate pos and neg labels
	getLabels(gradients, labels, hog_descriptors_pos, hog_descriptors_neg);

	//Convert  to one Mat (because each image needs to be one row)
	vecToMat(gradients, trainData);

	CreateTrainSVMmodel(trainData, labels);
}

//Reformats the Vector of Mat's to a single Mat for use in the SVM
void vecToMat(vector<Mat>& gradients, Mat& traindata)
{
	int rows = gradients.size();
	int cols = max(gradients[0].cols, gradients[0].rows);
	traindata = Mat(rows, cols, CV_32FC1);
	Mat tmp(1, cols, CV_32FC1);

	for (int i = 0; i < rows; i++)
	{
		if (gradients[i].cols != 1)
			gradients[i].copyTo(traindata.row(i));
		else {
			transpose(gradients[i], tmp);
			tmp.copyTo(traindata.row(i));
		}
	}
}

//Creates the SVM and saves it to my_birdo.yml
void CreateTrainSVMmodel(Mat & trainData, vector<int>& labels)
{
	Ptr<ml::SVM> svm = ml::SVM::create();
	svm->setCoef0(SVM_Coef);
	svm->setDegree(SVM_Degree);
	svm->setTermCriteria(TermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 1000, 1e-3));
	svm->setGamma(SVM_Gamma);
	svm->setKernel(SVM_kernelType);
	svm->setNu(SVM_Nu);
	svm->setP(SVM_P);
	svm->setC(SVM_C);
	svm->setType(SVM_type);

	Ptr<ml::TrainData> data = ml::TrainData::create(trainData, ml::ROW_SAMPLE, Mat(labels));
	svm->train(data);
	//svm->trainAuto(data);

	svm->save("my_birdo.yml");

}