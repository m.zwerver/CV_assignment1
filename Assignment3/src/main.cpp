#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>

#include "utilities/General.h"
#include "VoxelReconstruction.h"
#include "main.h"


using namespace nl_uu_science_gmt;



int main(int argc, char** argv)
{
	VoxelReconstruction::showKeys();
	VoxelReconstruction vr("data" + std::string(PATH_SEP), 4);
	//get camera parameters here
	//vr.writeCameraIntrinsics();
	//vr.writeBackgroundImages();
	//vr.saveFrames();

	vr.run(argc, argv);

	return EXIT_SUCCESS;
}
