/*
 * Reconstructor.h
 *
 *  Created on: Nov 15, 2013
 *      Author: coert
 */

#ifndef RECONSTRUCTOR_H_
#define RECONSTRUCTOR_H_

//#define CLUSTER5

#include <opencv2/core/core.hpp>
#include <stddef.h>
#include <vector>

#include "Camera.h"

namespace nl_uu_science_gmt
{

class Reconstructor
{
public:
	/*
	 * Voxel structure
	 * Represents a 3D pixel in the half space
	 */
	struct Voxel
	{
		int x, y, z;                               // Coordinates
		cv::Scalar color;                          // Color
		std::vector<cv::Point> camera_projection;  // Projection location for camera[c]'s FoV (2D)
		std::vector<int> valid_camera_projection;  // Flag if camera projection is in camera[c]'s FoV
	};

private:
	const std::vector<Camera*> &m_cameras;  // vector of pointers to cameras
	const int m_height;                     // Cube half-space height from floor to ceiling
	const int m_step;                       // Step size (space between voxels)

	std::vector<cv::Point3f*> m_corners;    // Cube half-space corner locations

	size_t m_voxels_amount;                 // Voxel count
	cv::Size m_plane_size;                  // Camera FoV plane WxH

	std::vector<Voxel*> m_voxels;           // Pointer vector to all voxels in the half-space
	std::vector<Voxel*> m_visible_voxels;   // Pointer vector to all visible voxels

	std::vector<cv::Mat> all_voxel_centers; // top row is "first cluster" etc
	cv::Mat voxel_centers;
	std::vector<int> m_voxel_labels;		//voxel labels found by clustering
	std::vector<int> best_orders;			//changed orders based on color models

	void initialize();

public:
	Reconstructor(const std::vector<Camera*> &);
	virtual ~Reconstructor();

	void update();

	const std::vector<Voxel*>& getVisibleVoxels() const
	{
		return m_visible_voxels;
	}

	const std::vector<Voxel*>& getVoxels() const
	{
		return m_voxels;
	}

	void setVisibleVoxels(const std::vector<Voxel*>& visibleVoxels)
	{
		m_visible_voxels = visibleVoxels;
	}

	void setVoxelLabels(std::vector<int> label_output)
	{
		m_voxel_labels = label_output;
	}

	std::vector<int> getVoxelLabels()
	{
		return m_voxel_labels;
	}

	void setVoxels(const std::vector<Voxel*>& voxels)
	{
		m_voxels = voxels;
	}

	void setBestOrders(std::vector<int> _best_orders)
	{
		best_orders = _best_orders;
	}

	std::vector<int> getBestOrders()
	{
		return best_orders;
	}

	void setVoxelCenters(cv::Mat& _voxel_centers)
	{
		voxel_centers = _voxel_centers;
		all_voxel_centers.push_back(_voxel_centers);
	}

	const cv::Mat& getVoxelCenters() const
	{
		return voxel_centers;
	}

	const std::vector<cv::Mat>& getAllVoxelCenters() const
	{
		return all_voxel_centers;
	}

	const std::vector<cv::Point3f*>& getCorners() const
	{
		return m_corners;
	}

	int getSize() const
	{
		return m_height;
	}

	const cv::Size& getPlaneSize() const
	{
		return m_plane_size;
	}
};

} /* namespace nl_uu_science_gmt */

#endif /* RECONSTRUCTOR_H_ */
