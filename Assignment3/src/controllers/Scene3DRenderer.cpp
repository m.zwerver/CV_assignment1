/*
 * Scene3DRenderer.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: coert
 */

#include "Scene3DRenderer.h"
#include "Reconstructor.h"

#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <stddef.h>
#include <string>
#include <iostream>

#include "../utilities/General.h"

using namespace std;
using namespace cv;

namespace nl_uu_science_gmt
{

/**
 * Constructor
 * Scene properties class (mostly called by Glut)
 */
Scene3DRenderer::Scene3DRenderer(
		Reconstructor &r, const vector<Camera*> &cs) :
				m_reconstructor(r),
				m_cameras(cs),
				m_num(4),
				m_sphere_radius(1850)
{
	m_width = 640;
	m_height = 480;
	m_quit = false;
	m_paused = false;
	m_rotate = false;
	m_camera_view = true;
	m_show_volume = true;
	m_show_grd_flr = true;
	m_show_cam = true;
	m_show_org = true;
	m_show_arcball = false;
	m_show_info = true;
	m_fullscreen = false;

	// Read the checkerboard properties (XML)
	FileStorage fs;
	fs.open(m_cameras.front()->getDataPath() + ".." + string(PATH_SEP) + General::CBConfigFile, FileStorage::READ);
	if (fs.isOpened())
	{
		fs["CheckerBoardWidth"] >> m_board_size.width;
		fs["CheckerBoardHeight"] >> m_board_size.height;
		fs["CheckerBoardSquareSize"] >> m_square_side_len;
	}
	fs.release();

	m_current_camera = 0;
	m_previous_camera = 0;

	m_number_of_frames = m_cameras.front()->getFramesAmount();
	m_current_frame = 0;
	m_previous_frame = -1;

	const int H = 0;
	const int S = 0;
	const int V = 0;
	m_h_threshold = H;
	m_ph_threshold = H;
	m_s_threshold = S;
	m_ps_threshold = S;
	m_v_threshold = V;
	m_pv_threshold = V;

	createTrackbar("Frame", VIDEO_WINDOW, &m_current_frame, m_number_of_frames - 2);
	createTrackbar("H", VIDEO_WINDOW, &m_h_threshold, 255);
	createTrackbar("S", VIDEO_WINDOW, &m_s_threshold, 255);
	createTrackbar("V", VIDEO_WINDOW, &m_v_threshold, 255);

	createFloorGrid();
	setTopView();


}

/**
 * Deconstructor
 * Free the memory of the floor_grid pointer vector
 */
Scene3DRenderer::~Scene3DRenderer()
{
	for (size_t f = 0; f < m_floor_grid.size(); ++f)
		for (size_t g = 0; g < m_floor_grid[f].size(); ++g)
			delete m_floor_grid[f][g];
}

//calculates which cluster belongs to which person according to the colormodel
void Scene3DRenderer::assignClusterCentersToPersons(Mat cluster_centers, Mat initial_color_model, Mat frame_color_model)
{
	vector<int> orders = { 0,1,2,3 };
	vector<int> best_orders;

	float best_squared_error = 780300; //max error: 255*255*12
	int type= CV_32FC1;

	Mat reordered_color_models = Mat(frame_color_model.rows, frame_color_model.cols, type);
	do
	{
		//reorder color models for all camers
		for (int c = 0; c < m_cameras.size(); c++)
		{
			for (int i = 0; i < orders.size(); i++)
			{
				//reordered_labels[i] = label_output[orders[i]];
				frame_color_model.row(orders[i] + (c * 4)).copyTo(reordered_color_models.row(i + (c * 4)));
			}
		}

		float sum_squared_error = 0;
#ifdef Mean
		for (int j = 0; j < reordered_color_models.rows; j++)// each center
		{
			float color_error = 0;
			for (int c = 0; c < reordered_color_models.cols; c++) //H
			{
				//cout << j << ", " << c << endl;
				float error = abs((reordered_color_models.at<float>(j, c)) - (initial_color_model.at<float>(j, c)));

				color_error += error;
			}
			sum_squared_error += color_error * color_error;
		}
#else
		for (int j = 0; j < reordered_color_models.rows; j++)// each histogram in each camera
		{
			float error = 1 - compareHist(initial_color_model.row(j), reordered_color_models.row(j), CV_COMP_CORREL);
			sum_squared_error += error;
		}
		
#endif
		if (sum_squared_error < best_squared_error)
		{
			best_squared_error = sum_squared_error;
			best_orders = orders;
		}

	} while (std::next_permutation(orders.begin(), orders.end()));

	Mat tmp = Mat(4,2,CV_32FC1);

	//reorder the voxel centers to best ordering
	for (int i = 0; i < cluster_centers.rows; i++)
	{
		for (int j = 0; j < cluster_centers.cols; j++)
		{
			tmp.at<float>(i, j) = cluster_centers.at<float>(best_orders[i], j);
		}
	}
	m_reconstructor.setVoxelCenters(tmp);
	m_reconstructor.setBestOrders(best_orders);
}

/**
 * Process the current frame on each camera
 */
bool Scene3DRenderer::processFrame()
{
	for (size_t c = 0; c < m_cameras.size(); ++c)
	{
		if (m_current_frame == m_previous_frame + 1)
		{
			m_cameras[c]->advanceVideoFrame();
		}
		else if (m_current_frame != m_previous_frame)
		{
			m_cameras[c]->getVideoFrame(m_current_frame);
		}
		assert(m_cameras[c] != NULL);

		processForeground(m_cameras[c]);
	}

	
	if (first_frame)
	{

		getBestHSVsmart(m_cameras);
		getBestErodeDilate(m_cameras[0]);
		Mat cluster_centers_tmp;
		vector<int> label_output_tmp;
		getColorModels(m_cameras, label_output_tmp, initial_color_model, true,cluster_centers_tmp);
		first_frame = false;
	}
	vector<int> label_output;

	Mat color_model;
	Mat cluster_centers;
	getColorModels(m_cameras, label_output,color_model, false,cluster_centers);

#ifndef CLUSTER5
	assignClusterCentersToPersons(cluster_centers, initial_color_model, color_model);
#endif

	return true;
}

//function for removing the voxels which represent the legs and head of a person
void cutOffLegs(vector<Reconstructor::Voxel*>& voxels, vector<int>& label_output)
{
	int max_z = 0;
	for (int i = 0; i < voxels.size(); i++)
	{
		Reconstructor::Voxel* v = voxels[i];
		int z = v->z;
		if (z > max_z)
			max_z = z;
	}

	//ratios estimated from images
	float estimated_leg_z = max_z * 0.45;
	float estimated_head_z = max_z * 0.75;

	vector<Reconstructor::Voxel*> voxels_tmp;
	vector<int> label_output_tmp;

	//remove pixels below estimated leg height or above estimated head height
	for (int i = 0; i < voxels.size(); i++)
	{
		Reconstructor::Voxel* v = voxels[i];
		if ((v->z < estimated_leg_z) || v->z > estimated_head_z)
		{
			voxels.erase(voxels.begin() + i);
			label_output.erase(label_output.begin() + i);
		}
		else
		{
			voxels_tmp.push_back(voxels[i]);
			label_output_tmp.push_back(label_output[i]);
		}
	}
	voxels = voxels_tmp;
	label_output = label_output_tmp;
}

//function for handling the creation of colormodels. return the colormodel and calculates the initial model if it is the first frame
void Scene3DRenderer::getColorModels(vector<Camera*> m_cameras, vector<int>& label_output, Mat& color_model, bool initial_model, Mat& cluster_centers)
{
	if (initial_model)
	{
		int frame_number = 1183;
		m_current_frame = frame_number;

		for (int i = 0; i < m_cameras.size(); i++)
		{
			m_cameras[i]->getVideoFrame(m_current_frame);
			processForeground(m_cameras[i]);
		}
	}
	clusterVoxels(cluster_centers, label_output);

	vector<Reconstructor::Voxel*> voxels = m_reconstructor.getVisibleVoxels();
	cutOffLegs(voxels, label_output);

#ifndef CLUSTER5
	CreateColorModel(label_output, color_model, voxels);
#endif

	if (initial_model)
	{
		//cout << " initial model: " << color_model << endl;
		//cout << " initial voxel_centers: " << cluster_centers << endl;
		for (int i = 0; i < m_cameras.size(); i++)
		{
			m_cameras[i]->getVideoFrame(0);
		}
		m_current_frame = 0;
	}
}

//calculates the color model of the current frame
void Scene3DRenderer::CreateColorModel(vector<int>& label_output, Mat& model, vector<Reconstructor::Voxel*> voxels)
{
#ifdef Mean
		vector<vector<float>> persons;
		vector<float> tmp;
		for (int i = 0; i < m_cameras.size() * 4; ++i)
			persons.push_back(tmp);

		for (int j = 0; j < m_cameras.size(); ++j)		
		{
			Mat frame = m_cameras[j]->getFrame();
			Mat HSV;
			cvtColor(frame, HSV, CV_BGR2HSV);
			vector<Mat> channels;
			split(HSV, channels);
			//cout << channels[0] << endl;
			for (int i = 0; i < voxels.size(); ++i)
			{
				if (voxels[i]->valid_camera_projection[j])
				{
					cv::Point location = voxels[i]->camera_projection[j];
					Vec3b color = HSV.at<cv::Vec3b>(location.y, location.x);
					
					Vec3f colorf = color;
					persons[label_output[i] + (j * 4)].push_back(colorf[0]);
				}
			}
		}

		model = Mat(m_cameras.size() * 4, 1, CV_32FC1);
		for (int i = 0; i < m_cameras.size() * 4; ++i)
		{
			float color = 0.0;
			for (int j = 0; j < persons[i].size(); ++j)
				color += persons[i][j];
			color = color / (int)persons[i].size();

			model.at<float>(i, 0) = color;
		}
#else
		model = Mat(m_cameras.size() * 4, 256, CV_32FC1);
		model.setTo(Scalar(0));

		
		for (int j = 0; j < m_cameras.size(); ++j)
		{
			Mat1b p1, p2, p3, p4;

			Mat frame = m_cameras[j]->getFrame();
			Mat HSV;
			cvtColor(frame, HSV, CV_BGR2HSV);
			vector<Mat> channels; //CV_8U1C
			split(HSV, channels);
			//cout << HSV << endl;
			for (int i = 0; i < voxels.size(); ++i)
			{
				if (voxels[i]->valid_camera_projection[j])
				{
					cv::Point location = voxels[i]->camera_projection[j];
					switch (label_output[i])
					{
					case 0:
						p1.push_back(channels[0].at<uchar>(location.y, location.x));
						break;
					case 1:
						p2.push_back(channels[0].at<uchar>(location.y, location.x));
						break;
					case 2:
						p3.push_back(channels[0].at<uchar>(location.y, location.x));
						break;
					case 3: 
						p4.push_back(channels[0].at<uchar>(location.y, location.x));
						break;
					}					
				}
			}

			int histSize = 256;

			/// Set the ranges ( for B,G,R) )
			float range[] = { 0, 256 };
			const float* histRange = { range };

			bool uniform = true; bool accumulate = false;
			Mat h_hist;

			calcHist(&p1, 1, 0, Mat(), h_hist, 1, &histSize, &histRange, uniform, accumulate);
			Mat tmp = h_hist.t();
			tmp.copyTo(model.row(j * 4));

			calcHist(&p2, 1, 0, Mat(), h_hist, 1, &histSize, &histRange, uniform, accumulate);
			tmp = h_hist.t();
			tmp.copyTo(model.row(1+j * 4));
			

			calcHist(&p3, 1, 0, Mat(), h_hist, 1, &histSize, &histRange, uniform, accumulate);
			tmp = h_hist.t();
			tmp.copyTo(model.row(2+j * 4));

			calcHist(&p4, 1, 0, Mat(), h_hist, 1, &histSize, &histRange, uniform, accumulate);
			tmp = h_hist.t();
			tmp.copyTo(model.row(3+j * 4) );
		}
#endif
}

//Finds the smallest cluster for a given label output, returns the index of this cluster and the percentage of voxels in it
void findSmallestCluster(vector<int> label_output, int num_clusters, float& smallest_voxel_per, int& smallest_index)
{
	vector<int> counts;
	for (int j = 0; j < num_clusters; j++)
	{
		counts.push_back(0);
	}

	for (int i = 0; i < label_output.size(); i++)
	{
		int label = label_output[i];
		counts[label]++;
	}

	int smallest_count = label_output.size();
	for (int s = 0; s < counts.size(); s++)
	{
		if (counts[s] < smallest_count)
		{
			smallest_count = counts[s];
			smallest_index = s;
		}

	}
	smallest_voxel_per = counts[smallest_index] / (float)label_output.size();
}

//Uses the current visible voxels and extracts the x and y coordinates to use as input for k-means
Mat Scene3DRenderer::getKMeansInput()
{
	vector<Reconstructor::Voxel*> voxels = m_reconstructor.getVisibleVoxels();
	Mat kmeans_input = Mat(voxels.size(), 2, CV_32F);

	for (int i = 0; i < voxels.size(); i++)
	{
		Reconstructor::Voxel* v = voxels[i];
		kmeans_input.row(i).col(0) = v->x;
		kmeans_input.row(i).col(1) = v->y;
	}
	return kmeans_input;
}

//Removes all voxels labels of the smallest cluster from the visible voxels and corresponding labels
void Scene3DRenderer::removeSmallestCluster(vector<int> label_output, int smallest_index)
{
	vector<Reconstructor::Voxel*> voxels = m_reconstructor.getVisibleVoxels();
	label_output = m_reconstructor.getVoxelLabels();

	vector<int> labels_new;
	vector<Reconstructor::Voxel*> voxels_new;

	//cout << "labels size: " << label_output.size() << endl;
	for (int i = 0; i < label_output.size(); i++)
	{
		if (label_output[i] == smallest_index)
		{

		}
		else
		{
			labels_new.push_back(label_output[i]);
			voxels_new.push_back(voxels[i]);
		}
	}
	m_reconstructor.setVisibleVoxels(voxels_new);
}

//Function that deletes the smallest cluster from (int clusters) number of clusters
void Scene3DRenderer::removeGhostVoxels(int clusters)
{
	Mat kmeans_input = getKMeansInput();
	vector<int> label_output_5 = vector<int>(kmeans_input.rows);
	Mat cluster_centers_5;

	TermCriteria tc;
	tc.epsilon = 1.0;
	tc.maxCount = 100;

	float smallest_voxel_per = 0;
	int smallest_index = 0;

	kmeans(kmeans_input, clusters, label_output_5, tc, 5, KMEANS_PP_CENTERS, cluster_centers_5);

	findSmallestCluster(label_output_5, clusters, smallest_voxel_per, smallest_index);
	//cout << "smallest index: " << smallest_index << endl;
	//cout << "smallest percentage: " << smallest_voxel_per << endl;

	m_reconstructor.setVoxelLabels(label_output_5);

	float t1_size_threshold = 1.0 / (clusters * 2);
	float t2_size_threshold = 2.5 / (clusters * clusters);

	if (smallest_voxel_per < t2_size_threshold) //threshold is important!
	{
		//cout << "removed" << endl ;
#ifndef CLUSTER5
		removeSmallestCluster(label_output_5, smallest_index);
#endif
	}
	//cout << endl;
}

//Clusters voxels and return the centers and labels to indicate to which center each voxel belongs.
void Scene3DRenderer::clusterVoxels(Mat& cluster_centers, vector<int>& label_output)
{
	m_reconstructor.update();
	
	TermCriteria tc;
	tc.epsilon = 1.0;
	tc.maxCount = 100;

	//removeGhostVoxels(9);
	//removeGhostVoxels(8);
	removeGhostVoxels(7);
	removeGhostVoxels(6);
	removeGhostVoxels(5);


	Mat kmeans_input = getKMeansInput();
	label_output = vector<int>(kmeans_input.rows);

#ifndef CLUSTER5
	kmeans(kmeans_input, 4, label_output, tc, 5, KMEANS_PP_CENTERS, cluster_centers);
	m_reconstructor.setVoxelLabels(label_output);
#endif
}

//for given hsv thresholds, returns the percentage of matching pixels between the extracted foreground and the corresponding ground truth
float testHSV(int h, int s, int v, vector<Mat> channels, Camera* camera, Mat best_foreground)
{
	// Background subtraction H
	Mat tmp, foreground, background;

	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, s, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, v, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	//compare found foreground with true foreground
	Mat difference;
	absdiff(foreground, best_foreground, difference);
	int mistakes = countNonZero(difference);
	float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

	//cout << "for: h = " << h << ", s = " << s << ", v = " << v << ", mistakes = " << mistakes << ", match = " << hit_per << endl;
	return hit_per;

}

//guesses hsv values for the first camera with corresponding ground truth
void guessHSV(int& best_h, int& best_s, int& best_v, int tries, vector<Mat> channels, Camera* camera, Mat best_foreground)
{
	float best_per = 0;
	int current_best_value;
	for (int i = 0; i < tries; i++)
	{
		int hsv_value = (255.0 / tries) * i;
		
		float hit_per = testHSV(hsv_value, hsv_value, hsv_value, channels, camera, best_foreground);
		if (hit_per > best_per)
		{
			best_per = hit_per;
			current_best_value = hsv_value;
		}

	}

	best_h = current_best_value;
	best_s = current_best_value;
	best_v = current_best_value;
}

//gets the frame that is used for the hsv and erode/dilate algorithms
Mat getForegroundFrame(Camera* camera)
{
	Mat img;
	String video_path = camera->getDataPath() + "video.avi";
	VideoCapture cap(video_path);
	float time = 23.727 / 54.483;

	cap.set(CV_CAP_PROP_POS_AVI_RATIO, 1);  // Go to the end of the video; 1 = 100%
	long num_frames = (long)cap.get(CV_CAP_PROP_POS_FRAMES);
	cap.set(CV_CAP_PROP_POS_AVI_RATIO, 0);  // Go to the start of the video; 0 = 0%
	
	cap.set(CV_CAP_PROP_POS_FRAMES, time * num_frames - 3);
	cap >> img;
	return img;
}


//calculates the optimal hsv values by guessing hsv values and then adjusting the values in the direction of the highest gain
void Scene3DRenderer::getBestHSVsmart(vector<Camera*> cameras)
{
	Mat best_foreground_1 = imread(cameras[0]->getDataPath() + "tf_frame3_cam1.png", CV_LOAD_IMAGE_GRAYSCALE);
	
	Mat current_frame1, current_frame2, current_frame3, current_frame4;
	Mat img = getForegroundFrame(cameras[0]);
	cvtColor(img, current_frame1, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels1, channels2, channels3, channels4;
	split(current_frame1, channels1);  // Split the HSV-channels for further analysis

	int tries = 0;
	int best_h;
	int best_s;
	int best_v;

	guessHSV(best_h, best_s, best_v, 10, channels1, cameras[0], best_foreground_1);

	float best_hit_rate = 0;
	bool progress = true;
	int max_tries = 10; //this one is important for escaping local minima (if there are any). in this particular case, 9 is the minimum to get the optimal solution
	int step_size = 1;

	while (progress)
	{
		int best_h_round;
		int best_s_round;
		int best_v_round;
		
		float best_per_round = best_hit_rate;

		for (int h_step = -1; h_step <= 1; h_step += 1)
		{
			for (int s_step = -1; s_step <= 1; s_step += 1)
			{
				for (int v_step = -1; v_step <= 1; v_step += 1)
				{
					int h = best_h + h_step * step_size;
					int s = best_s + s_step * step_size;
					int v = best_v + v_step * step_size;

					if (!((h < 0) || (h > 255) || (s < 0) || (s > 255) || (v < 0) || (v > 255)))
					{
						float hit_per = testHSV(h, s, v, channels1, cameras[0], best_foreground_1);
						tries++;
						if (hit_per > best_per_round)
						{
							best_h_round = h;
							best_s_round = s;
							best_v_round = v;
							best_per_round = hit_per;
						}
					}
				}
			}
		}

		if (best_per_round > best_hit_rate)
		{
			best_h = best_h_round;
			best_s = best_s_round;
			best_v = best_v_round;
			best_hit_rate = best_per_round;
			step_size = 1;
		}
		else
		{
			step_size++;
			if(step_size > max_tries)
				progress = false;
		}
	}
	cout << "best hsv: " << best_h << ", " << best_s << ", " << best_v << ", rate: " << best_hit_rate << ", tries: " << tries << endl;
	m_h_threshold = best_h;
	m_ph_threshold = best_h;
	m_s_threshold = best_s;
	m_ps_threshold = best_s;
	m_v_threshold = best_v;
	m_pv_threshold = best_v;

	createTrackbar("Frame", VIDEO_WINDOW, &m_current_frame, m_number_of_frames - 2);
	createTrackbar("H", VIDEO_WINDOW, &m_h_threshold, 255);
	createTrackbar("S", VIDEO_WINDOW, &m_s_threshold, 255);
	createTrackbar("V", VIDEO_WINDOW, &m_v_threshold, 255);
}

//Finds best erode and dilate values
void Scene3DRenderer::getBestErodeDilate(Camera * camera)
{
	String path = camera->getDataPath();
	//cout << path;
	Mat best_foreground = imread(path + "tf_frame3_cam1.png", CV_LOAD_IMAGE_GRAYSCALE);

	//imshow("ground truth", best_foreground);
	Mat current_frame;

	Mat img = getForegroundFrame(camera);
	cvtColor(img, current_frame, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels, channels_best;
	split(current_frame, channels);  // Split the HSV-channels for further analysis

	int h = m_h_threshold;
	int s = m_s_threshold;
	int v = m_v_threshold;
	float best_hit_rate = 0;


	int best_erode_it = 0;
	int best_dilate_it = 0;
	int best_erode = 0;
	int best_dilate = 0;

	
	// Background subtraction H
	Mat tmp, foreground, background;

	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, s, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, v, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	for (int e_iterate = 0; e_iterate < 10; e_iterate++)
		for (int d_iterate = 0; d_iterate < 10; d_iterate++)
			for(int e =0; e < 10;e++)
				for(int d = 0; d < 10; d++)
				{
					int ane = e;
					int and = d;
					Mat elemente = getStructuringElement(MORPH_ELLIPSE, Size(ane + 1, ane + 1), Point(ane, ane));
					Mat elementd = getStructuringElement(MORPH_RECT, Size(and + 1, and + 1), Point(and, and));
					for(int i =0; i < e_iterate;i++)
						erode(foreground, tmp, elemente);
					for (int i = 0; i < d_iterate; i++)
						dilate(tmp, tmp, elementd);

					//compare found foreground with true foreground
					Mat difference;
					absdiff( best_foreground,tmp, difference);
					int mistakes = countNonZero(difference);
					float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

					if (hit_per > best_hit_rate)
					{
						best_erode_it = e_iterate;
						best_dilate_it = d_iterate;
						best_erode = e;
						best_dilate = d;
						best_hit_rate = hit_per;
					}
				}	
	cout << "best: e_iterate = " << best_erode_it << ", d_iterate = " << best_dilate_it  << ", e =" << best_erode << ", d =" << best_dilate << ", match = " << best_hit_rate << endl;

	erodeSize = best_erode;
	erode_iterations = best_erode_it;
	dilateSize = best_dilate;
	dilate_iterations = best_dilate_it;
}

//gets the optimal hsv values using brute force (don't run, takes forever)
void Scene3DRenderer::getBestHSV(Camera* camera)
{
	String path = camera->getDataPath();
	cout << path;
	Mat best_foreground = imread(path + "true_foreground1.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat current_frame;

	assert(!camera->getFrame().empty());
	cvtColor(camera->getFrame(), current_frame, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels, channels_best;
	split(current_frame, channels);  // Split the HSV-channels for further analysis

	int best_h = 0;
	int best_s = 0;
	int best_v = 0;
	float best_hit_rate = 0;

	for (int h = 0; h < 255; h++)
	{
		for (int s = 0; s < 255; s++)
		{
			for (int v = 0; v < 255; v++)
			{
				// Background subtraction H
				Mat tmp, foreground, background;

				absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
				threshold(tmp, foreground, h, 255, CV_THRESH_BINARY);

				// Background subtraction S
				absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
				threshold(tmp, background, s, 255, CV_THRESH_BINARY);
				bitwise_and(foreground, background, foreground);

				// Background subtraction V
				absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
				threshold(tmp, background, v, 255, CV_THRESH_BINARY);
				bitwise_or(foreground, background, foreground);

				//compare found foreground with true foreground
				Mat difference;
				absdiff(foreground, best_foreground, difference);
				int mistakes = countNonZero(difference);
				float hit_per = (1 - ((double)mistakes / (background.rows * background.cols))) * 100;

				if (hit_per > best_hit_rate)
				{
					best_h = h;
					best_s = s;
					best_v = v;
					best_hit_rate = hit_per;
				}

			}
		}
	}
	cout << "Best:for: h = " << best_h << ", s = " << best_s << ", v = " << best_v << ", match = " << best_hit_rate/2 << endl;
	m_h_threshold = best_h;
	m_ph_threshold = best_h;
	m_s_threshold = best_s;
	m_ps_threshold = best_s;
	m_v_threshold = best_v;
	m_pv_threshold = best_v;
}

/**
 * Separate the background from the foreground
 * ie.: Create an 8 bit image where only the foreground of the scene is white (255)
 */
void Scene3DRenderer::processForeground(Camera* camera)
{
	//imwrite("image.png", camera->getFrame());
	assert(!camera->getFrame().empty());
	Mat hsv_image;
	cvtColor(camera->getFrame(), hsv_image, CV_BGR2HSV);  // from BGR to HSV color space

	vector<Mat> channels;
	split(hsv_image, channels);  // Split the HSV-channels for further analysis

	// Background subtraction H
	Mat tmp, foreground, background;
	absdiff(channels[0], camera->getBgHsvChannels().at(0), tmp);
	threshold(tmp, foreground, m_h_threshold, 255, CV_THRESH_BINARY);

	// Background subtraction S
	absdiff(channels[1], camera->getBgHsvChannels().at(1), tmp);
	threshold(tmp, background, m_s_threshold, 255, CV_THRESH_BINARY);
	bitwise_and(foreground, background, foreground);

	// Background subtraction V
	absdiff(channels[2], camera->getBgHsvChannels().at(2), tmp);
	threshold(tmp, background, m_v_threshold, 255, CV_THRESH_BINARY);
	bitwise_or(foreground, background, foreground);

	// Improve the foreground image
	Mat elemente = getStructuringElement(MORPH_ELLIPSE, Size(erodeSize + 1, erodeSize + 1), Point(erodeSize, erodeSize));
	Mat elementd = getStructuringElement(MORPH_RECT, Size(dilateSize + 1, dilateSize + 1), Point(dilateSize, dilateSize));

	for (int i = 0; i < erode_iterations; i++)
		erode(foreground, foreground, elemente);
	for (int i = 0; i < dilate_iterations+1; i++)
		dilate(foreground, foreground, elementd);

	camera->setForegroundImage(foreground);
	//imshow("foreground", foreground);

}

/**
 * Set currently visible camera to the given camera id
 */
void Scene3DRenderer::setCamera(int camera)
{
	m_camera_view = true;

	if (m_current_camera != camera)
	{
		m_previous_camera = m_current_camera;
		m_current_camera = camera;
		m_arcball_eye.x = m_cameras[camera]->getCameraPlane()[0].x;
		m_arcball_eye.y = m_cameras[camera]->getCameraPlane()[0].y;
		m_arcball_eye.z = m_cameras[camera]->getCameraPlane()[0].z;
		m_arcball_up.x = 0.0f;
		m_arcball_up.y = 0.0f;
		m_arcball_up.z = 1.0f;
	}
}

/**
 * Set the 3D scene to bird's eye view
 */
void Scene3DRenderer::setTopView()
{
	m_camera_view = false;
	if (m_current_camera != -1)
		m_previous_camera = m_current_camera;
	m_current_camera = -1;

	m_arcball_eye = vec(0.0f, 0.0f, 10000.0f);
	m_arcball_centre = vec(0.0f, 0.0f, 0.0f);
	m_arcball_up = vec(0.0f, 1.0f, 0.0f);
}

/**
 * Create a LUT for the floor grid
 */
void Scene3DRenderer::createFloorGrid()
{
	const int size = m_reconstructor.getSize() / m_num;
	const int z_offset = 3;

	// edge 1
	vector<Point3i*> edge1;
	for (int y = -size * m_num; y <= size * m_num; y += size)
		edge1.push_back(new Point3i(-size * m_num, y, z_offset));

	// edge 2
	vector<Point3i*> edge2;
	for (int x = -size * m_num; x <= size * m_num; x += size)
		edge2.push_back(new Point3i(x, size * m_num, z_offset));

	// edge 3
	vector<Point3i*> edge3;
	for (int y = -size * m_num; y <= size * m_num; y += size)
		edge3.push_back(new Point3i(size * m_num, y, z_offset));

	// edge 4
	vector<Point3i*> edge4;
	for (int x = -size * m_num; x <= size * m_num; x += size)
		edge4.push_back(new Point3i(x, -size * m_num, z_offset));

	m_floor_grid.push_back(edge1);
	m_floor_grid.push_back(edge2);
	m_floor_grid.push_back(edge3);
	m_floor_grid.push_back(edge4);
}




} /* namespace nl_uu_science_gmt */
