/*
 * VoxelReconstruction.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: coert
 */

#include "VoxelReconstruction.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <stddef.h>
#include <cassert>
#include <iostream>
#include <sstream>
#include <thread>

#include "controllers/Glut.h"
#include "controllers/Reconstructor.h"
#include "controllers/Scene3DRenderer.h"
#include "utilities/General.h"
#include "camera_calibration.h";

using namespace nl_uu_science_gmt;
using namespace std;
using namespace cv;

namespace nl_uu_science_gmt
{

/**
 * Main constructor, initialized all cameras
 */
VoxelReconstruction::VoxelReconstruction(const string &dp, const int cva) :
		m_data_path(dp), m_cam_views_amount(cva)
{
	const string cam_path = m_data_path + "cam";

	for (int v = 0; v < m_cam_views_amount; ++v)
	{
		stringstream full_path;
		full_path << cam_path << (v + 1) << PATH_SEP;

		/*
		 * Assert that there's a background image or video file and \
		 * that there's a video file
		 */
		std::cout << full_path.str() << General::BackgroundImageFile << std::endl;
		std::cout << full_path.str() << General::VideoFile << std::endl;
		assert(
			General::fexists(full_path.str() + General::BackgroundImageFile)
			&&
			General::fexists(full_path.str() + General::VideoFile)
		);

		/*
		 * Assert that if there's no config.xml file, there's an intrinsics file and
		 * a checkerboard video to create the extrinsics from
		 */
		assert(
			(!General::fexists(full_path.str() + General::ConfigFile) ?
				General::fexists(full_path.str() + General::IntrinsicsFile) &&
					General::fexists(full_path.str() + General::CheckerboadVideo)
			 : true)
		);

		m_cam_views.push_back(new Camera(full_path.str(), General::ConfigFile, v));
	}
}

/**
 * Main destructor, cleans up pointer vector memory of the cameras
 */
VoxelReconstruction::~VoxelReconstruction()
{
	for (size_t v = 0; v < m_cam_views.size(); ++v)
		delete m_cam_views[v];
}

/**
 * What you can hit
 */
void VoxelReconstruction::showKeys()
{
	cout << "VoxelReconstruction v" << VERSION << endl << endl;
	cout << "Use these keys:" << endl;
	cout << "q       : Quit" << endl;
	cout << "p       : Pause" << endl;
	cout << "b       : Frame back" << endl;
	cout << "n       : Next frame" << endl;
	cout << "r       : Rotate voxel space" << endl;
	cout << "s       : Show/hide arcball wire sphere (Linux only)" << endl;
	cout << "v       : Show/hide voxel space box" << endl;
	cout << "g       : Show/hide ground plane" << endl;
	cout << "c       : Show/hide cameras" << endl;
	cout << "i       : Show/hide camera numbers (Linux only)" << endl;
	cout << "o       : Show/hide origin" << endl;
	cout << "t       : Top view" << endl;
	cout << "1,2,3,4 : Switch camera #" << endl << endl;
	cout << "Zoom with the scrollwheel while on the 3D scene" << endl;
	cout << "Rotate the 3D scene with left click+drag" << endl << endl;
}

/**
 * - If the xml-file with camera intrinsics, extrinsics and distortion is missing,
 *   create it from the checkerboard video and the measured camera intrinsics
 * - After that initialize the scene rendering classes
 * - Run it!
 */
void VoxelReconstruction::run(int argc, char** argv)
{
	for (int v = 0; v < m_cam_views_amount; ++v)
	{
		bool has_cam = Camera::detExtrinsics(m_cam_views[v]->getDataPath(), General::CheckerboadVideo,
				General::IntrinsicsFile, m_cam_views[v]->getCamPropertiesFile(),v, false);
		assert(has_cam);
		if (has_cam) has_cam = m_cam_views[v]->initialize();
		assert(has_cam);
	}

	destroyAllWindows();
	namedWindow(VIDEO_WINDOW, CV_WINDOW_KEEPRATIO);

	Reconstructor reconstructor(m_cam_views);
	Scene3DRenderer scene3d(reconstructor, m_cam_views);
	Glut glut(scene3d);

#ifdef __linux__
	glut.initializeLinux(SCENE_WINDOW.c_str(), argc, argv);
#elif defined _WIN32
	glut.initializeWindows(SCENE_WINDOW.c_str());
	glut.mainLoopWindows();
#endif
}

//calculates the camera intrinsics using the code from assignment 1
void VoxelReconstruction::calcCameraIntrinsics(Camera *c, Mat_<float>& camera_matrix, Mat_<float> &dist_coeffs)
{
	calibrationMain(c, camera_matrix, dist_coeffs);
}

//todo
void VoxelReconstruction::writeCameraIntrinsics()
{
	//something goes wrong with the location, but projection is correct
	if (false)
	{

		thread t[4];

		for (int i = 0; i < 4; ++i) {
			t[i] = thread(&VoxelReconstruction::writeCameraIntrinsic, this, i);
		}

		for (int i = 0; i < 4; ++i) {
			t[i].join();
		}
	}
	else
	{
		int v ;
		#pragma omp parallel for schedule(static) private(v)
		for (v = 0; v < m_cam_views_amount; v++)
		{

			string cam_path = m_data_path + "cam" + to_string(v+1) + "/intrinsics2.xml";


			FileStorage fs;
			fs.open(cam_path, FileStorage::WRITE);
			if (fs.isOpened())
			{
				Mat_<float> camera_matrix = Mat_<float>::eye(3, 3);
				Mat_<float> dist_coeffs = Mat_<float>::zeros(5, 1);

				Camera *c = m_cam_views.at(v);

				calcCameraIntrinsics(c, camera_matrix, dist_coeffs);

				fs << "CameraMatrix" << camera_matrix;
				fs << "DistortionCoeffs" << dist_coeffs;

				fs.release();
			}
		}
	}

}

//gets camera intrinsics and writes the to intrinsics2.xml in the specified format (not used?)
void VoxelReconstruction::writeCameraIntrinsic(int i)
{
	string cam_path = m_data_path + "cam" + to_string(i + 1) + "/intrinsics2.xml";
	
	FileStorage fs;
	fs.open(cam_path, FileStorage::WRITE);
	if (fs.isOpened())
	{
		Mat_<float> camera_matrix = Mat_<float>::eye(3, 3);
		Mat_<float> dist_coeffs = Mat_<float>::zeros(5, 1);

		Camera *c = m_cam_views.at(i);

		calcCameraIntrinsics(c, camera_matrix, dist_coeffs);

		fs << "CameraMatrix" << camera_matrix;
		fs << "DistortionCoeffs" << dist_coeffs;

		fs.release();
	}
}

void VoxelReconstruction::saveFrames() //best frame = frame 3?
{
	for (int v = 0; v < m_cam_views_amount; v++)
	{
		Camera *c = m_cam_views.at(v);
		Mat img;
		String video_path = c->getDataPath() + "video.avi";
		VideoCapture cap(video_path);
		if (!cap.isOpened())  // check if we succeeded
			return;

		float time = 23.727 / 54.483; //23.727 is too late

		cap.set(CV_CAP_PROP_POS_AVI_RATIO, 1);  // Go to the end of the video; 1 = 100%
		long num_frames = (long)cap.get(CV_CAP_PROP_POS_FRAMES);
		cap.set(CV_CAP_PROP_POS_AVI_RATIO, 0);  // Go to the start of the video; 0 = 0%

		for (int i = 0; i < 10; i++)
		{
			if (!cap.set(CV_CAP_PROP_POS_FRAMES, time * num_frames - i))
				cout << "Error!";

			String write_path = m_data_path + "cam" + to_string(v + 1) + "/frame" + to_string(i) + ".png";
			cap >> img;
			imwrite(write_path, img);
		}
	}
}

//takes certain percentage of frames from video and averages them to get the background image
void getBackground(Camera *c, Mat& img)
{
	String video_path = c->getDataPath() + "background.avi";

	VideoCapture cap(video_path);
	if (!cap.isOpened())  // check if we succeeded
		return;
	
	cap.set(CV_CAP_PROP_POS_AVI_RATIO, 1);  // Go to the end of the video; 1 = 100%
	long num_frames = (long) cap.get(CV_CAP_PROP_POS_FRAMES);
	cap.set(CV_CAP_PROP_POS_AVI_RATIO, 0);  // Go to the start of the video; 0 = 0%

	double per = 1.0; //percentage of frames used for background
	int skip_frames = (int)(1 / per);

	int frame_width = (int)cap.get(CV_CAP_PROP_FRAME_WIDTH);
	int frame_height = (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT);

	Mat sum_img = Mat::zeros(frame_height, frame_width, CV_32SC3); //3 channel int
	Mat frame = Mat::zeros(frame_height, frame_width, CV_8UC3); //3 channel uchar (255 limit causing mistakes)
	int num_img = 0;

	for (int i = 0; i <= num_frames; i += skip_frames)
	{
		if (!cap.set(CV_CAP_PROP_POS_FRAMES, skip_frames* num_img )) 
			cout << "Error!";

		cap >> frame;
		Mat conv_frame = Mat(frame_height, frame_width, CV_32SC3);
		frame.convertTo(conv_frame, CV_32SC3);
		Mat summed_img = Mat::zeros(frame_height, frame_width, CV_32SC3);
		sum_img = sum_img + conv_frame;
		num_img++;
	}

	img = sum_img / num_img;
	img.convertTo(img, CV_8UC3);
}

//saves file called background.png for each camera using getBackground()
void VoxelReconstruction::writeBackgroundImages()
{
	for (int v = 0; v < m_cam_views_amount; v++)
	{
		Mat img;
		Camera *c = m_cam_views.at(v);

		getBackground(c, img);

		String write_path = m_data_path + "cam" + to_string(v + 1) + "/background.png";
		imwrite(write_path, img);
		
	}
}
} /* namespace nl_uu_science_gmt */
